<?php

use console\controllers\MigrateController;

$params = array_merge(
    require __DIR__ . '/../../api/config/params.php',
    require __DIR__ . '/../../api/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'params'              => $params,
    'controllerMap'       => [
        'migrate' => [
            'class'         => MigrateController::class,
            // additional aliases of migration directories
            // 'migrationLookup' => [],
            'migrationPath' => '@common/migrations',
            'templateFile'  => '@console/views/migration.php',
        ],
    ],
];
