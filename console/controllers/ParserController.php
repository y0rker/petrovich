<?php
/**
 * User: y0rker
 * Date: 12.12.18
 * Time: 18:05
 */

namespace console\controllers;

use common\components\exceptions\ValidationException;
use common\services\parsers\LeruaService\LeruaCatalogService;
use common\services\parsers\LeruaService\LeruaCategoryService;
use common\services\parsers\PetrovichService\PetrovichCatalogService;
use common\services\parsers\PetrovichService\PetrovichCategoryService;
use common\services\parsers\PetrovichService\PetrovichService;
use common\services\parsers\ParserSantechluxService\SantechluxCategoryService;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class ParserController
 * @package console\controllers
 */
class ParserController extends Controller
{
    /**
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionPetrovichCatalogMaterials(): void
    {
        PetrovichCatalogService::getInstance()
            ->run();
    }

    /**
     * @throws InvalidConfigException
     */
    public function actionLeruaCatalogMaterials(): void
    {
        LeruaCatalogService::getInstance()
            ->run();
    }

    /**
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionParsePetrovich(): void
    {
        PetrovichService::getInstance()
            ->run();
    }

    /**
     * @throws ValidationException
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionPetrovichCategories(): void
    {
        PetrovichCategoryService::getInstance()
            ->run();
    }

    /**
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionParseSantechluxCategories(): void
    {
        SantechluxCategoryService::getInstance()
            ->run();
    }

    /**
     * @param string $clientName
     * @throws InvalidConfigException
     */
    public function actionParse(string $clientName): void
    {
        $parsers = Yii::$app->parsers;
        $client = $parsers->getClient($clientName);
        $service = $client->getService();
        $service->run();
    }

    /**
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionLeruaCategories(): void
    {
        LeruaCategoryService::getInstance()
            ->run();
    }
}
