<?php
/**
 * Created by PhpStorm.
 * Module: andkon
 * Date: 03.05.16
 * Time: 9:25
 */

namespace console\controllers;

// @codingStandardsIgnoreStart
use common\dto\mysql\CategoryDao;
use common\dto\mysql\CollectionItemDao;
use common\dto\mysql\CollectionsDao;
use common\dto\mysql\FilmsDao;
use common\dto\mysql\SyncCountHistoryDao;
use common\dto\mysql\TestDao;
use common\dto\mysql\VideoSourcesDao;
use common\models\Category;
use common\models\Collection;
use common\models\CollectionItem;
use common\models\Film;
use common\models\SyncCountHistory;
use common\models\Test;
use common\models\VideoSource;
use Yii;

error_reporting('~' . E_WARNING);
// @codingStandardsIgnoreEnd

/**
 * Class MigrateController
 *
 * @package app\console\controllers
 */
class MigrateController extends \webtoucher\migrate\controllers\MigrateController
{
    /**
     * исправлено вычесление пути
     *
     * @param string      $name   имя файла
     * @param null|string $module имя модкля
     *
     * @return void
     * @throws \Exception
     */
    public function actionCreate($name, $module = null)
    {
        if (!empty($module)) {
            $migrationPath = $this->modulesPath . DIRECTORY_SEPARATOR . $module;
            $path          = \Yii::getAlias($migrationPath);
            if (!is_dir($path)) {
                throw new \Exception("Module '$module' is not exists");
            }
            $path = "$path/migrations";
            if (!is_dir($path)) {
                mkdir($path);
            }
            $this->migrationPath = $path;
        }
        if (!preg_match('/^\w+$/', $name)) {
            throw new \Exception(
                'The migration name should contain letters, digits and/or underscore characters only.'
            );
        }

        $name = 'm' . gmdate('ymd_His') . '_' . $name;
        $file = \Yii::getAlias($this->migrationPath) . DIRECTORY_SEPARATOR . $name . '.php';

        if ($this->confirm("Create new migration '$file'?")) {
            $content = $this->renderFile(\Yii::getAlias($this->templateFile), ['className' => $name]);
            file_put_contents(\Yii::getAlias($file), $content);
            echo "New migration created successfully.\n";
        }
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateCategory(): void
    {
        /** @var CategoryDao[] $categories */
        $categories = (new \yii\db\Query())
            ->from(['categories'])
            ->select('*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, CategoryDao::class]);

        $date = date('Y-m-d H:i:s');
        $insertData = [];
        foreach ($categories as $category) {
            $insertData[] = [
                $category->NAME,
                $category->DESCRIPTION,
                $category->MW_FULL_URL,
                $category->MW_UPDATE_URL,
                $date
            ];
        }

        Category::deleteAll();
        Yii::$app->getDb()->createCommand()->resetSequence(Category::tableName())->execute();
        Yii::$app->getDb()->createCommand()->batchInsert(Category::tableName(), [
            'name',
            'description',
            'mw_full_url',
            'mw_update_url',
            'created'
        ], $insertData)->execute();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    public function actionMigrateMysql(): void
    {
        $this->migrateCategory();
        $this->migrateSyncCountHistory();
        $this->migrateTest();
        $this->migrateVideo();
        $this->migrateFilm();
        $this->migrateCollections();
        $this->migrateCollectionItem();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateSyncCountHistory(): void
    {
        /** @var SyncCountHistoryDao[] $syncs */
        $syncs = (new \yii\db\Query())
            ->from(['sync_count_history'])
            ->select('*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, SyncCountHistoryDao::class]);

        $insertData = [];
        foreach ($syncs as $sync) {
            $insertData[] = [
                $sync->TIMESTAMP,
                $sync->FILMS_COUNT,
                $sync->VIDEO_COUNT,
                $sync->SOURCE,
                $sync->SYNC_TIME
            ];
        }

        SyncCountHistory::deleteAll();
        Yii::$app->getDb()->createCommand()->resetSequence(SyncCountHistory::tableName())->execute();
        Yii::$app->getDb()->createCommand()->batchInsert(SyncCountHistory::tableName(), [
            'created',
            'films_count',
            'video_count',
            'source',
            'sync_time',
        ], $insertData)->execute();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateTest(): void
    {
        /** @var TestDao[] $tests */
        $tests = (new \yii\db\Query())
            ->from(['tests'])
            ->select('*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, TestDao::class]);

        $insertData = [];
        foreach ($tests as $test) {
            $insertData[] = [
                $test->FRAME,
                $test->IMAGE,
                $test->NAME,
                $test->DESCRIPTION,
                $test->DATE_CREATE,
                $test->DATE_UPDATE,
                $test->ACTIVE
            ];
        }
        Test::deleteAll();
        Yii::$app->getDb()->createCommand()->resetSequence(Test::tableName())->execute();
        Yii::$app->getDb()->createCommand()->batchInsert(Test::tableName(), [
            'frame',
            'image',
            'name',
            'description',
            'created',
            'updated',
            'visible'
        ], $insertData)->execute();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateVideo(): void
    {
        /** @var VideoSourcesDao[] $videos */
        $videos = (new \yii\db\Query())
            ->from(['vs' => 'video_sources'])
            ->select('vs.*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, VideoSourcesDao::class]);

        $date = date('Y-m-d H:i:s');
        $insertData = [];
        foreach ($videos as $video) {
            $code = trim(str_replace('.', '', $video->NAME));
            $insertData[] = [
                $video->NAME,
                $code,
                $date
            ];
        }
        VideoSource::deleteAll();
        Yii::$app->getDb()->createCommand()->resetSequence(VideoSource::tableName())->execute();
        Yii::$app->getDb()->createCommand()->batchInsert(VideoSource::tableName(), [
            'name',
            'code',
            'created'
        ], $insertData)->execute();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateCollections(): void
    {
        /** @var CollectionsDao[] $collections */
        $collections = (new \yii\db\Query())
            ->from(['collections'])
            ->select('*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, CollectionsDao::class]);

        $insertData = [];
        foreach ($collections as $collection) {
            $insertData[] = [
                $collection->NAME,
                $collection->DESCRIPTION,
                $collection->IMAGE,
                $collection->ACTIVE,
                $collection->DATE_CREATE,
                $collection->DATE_UPDATE
            ];
        }

        Collection::deleteAll();
        Yii::$app->getDb()->createCommand()->resetSequence(Collection::tableName())->execute();
        Yii::$app->getDb()->createCommand()->batchInsert(Collection::tableName(), [
            'name',
            'description',
            'image',
            'visible',
            'created',
            'updated'
        ], $insertData)->execute();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateCollectionItem(): void
    {
        /** @var CollectionItemDao[] $items */
        $items = (new \yii\db\Query())
            ->from(['collection_films'])
            ->select('*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, CollectionItemDao::class]);

        $insertData = [];
        foreach ($items as $item) {
            $insertData[] = [
                $item->COLLECTION_ID,
                $item->KP_ID,
            ];
        }

        CollectionItem::deleteAll();
        Yii::$app->getDb()->createCommand()->batchInsert(CollectionItem::tableName(), [
            'collection_id',
            'film_kp_id'
        ], $insertData)->execute();
    }

    /**
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function migrateFilm(): void
    {
        /** @var FilmsDao[] $films */
        $films = (new \yii\db\Query())
            ->from(['films'])
            ->select('*')
            ->createCommand(Yii::$app->dbMysql)
            ->queryAll([\PDO::FETCH_CLASS, FilmsDao::class]);

        $date = date('Y-m-d H:i:s');
        $insertData = [];
        foreach ($films as $film) {
            if ($film->CATEGORY_ID === '0') {
                $film->CATEGORY_ID = null;
            }
            $data = [
                $film->TITLE_RU,
                $film->TITLE_EN,
                $film->POSTER,
                $film->DESCRIPTION,
                $film->KP_DESCRIPTION,
                $film->TRANSLATOR,
                $film->QUANTITY,
                $film->YEAR,
                $film->KP_ID,
                $film->WA_ID,
                $film->IMDB_ID,
                $film->SOURCE_ID,
                $film->TYPE,
                $film->SOURCE_URL,
                $film->SOURCE_TOKEN,
                $film->CATEGORY_ID,
                $film->PARENT_ID,
                $film->HIDDEN,
                $film->ACTIVE,
                $date
            ];
            $insertData[] = $data;
        }

        Film::deleteAll();
        Yii::$app->getDb()->createCommand()->resetSequence(Film::tableName())->execute();
        $filmsChunk = array_chunk($insertData, 100);
        foreach ($filmsChunk as $insert) {
            Yii::$app->getDb()->createCommand()->batchInsert(Film::tableName(), [
                'title_ru',
                'title_en',
                'poster',
                'description',
                'description_kp',
                'translator',
                'quantity',
                'year',
                'kp_id',
                'wa_id',
                'imdb_id',
                'video_source_id',
                'type',
                'video_source_url',
                'video_source_token',
                'category_id',
                'parent_id',
                'is_hidden',
                'visible',
                'created'
            ], $insert)->execute();
        }
    }
}
