<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 17:16
 */

namespace common\jobs;

use common\components\Jobs;
use common\dto\ParserMaterialCategoryDto;
use common\services\ParserMaterialsServiceBase;
use Throwable;
use Yii;
use yii\queue\JobInterface;
use yii\queue\Queue;

/**
 * Class ParseMaterialsJob
 * @package common\jobs
 */
class ParseMaterialsJob extends Jobs implements JobInterface
{
    /** @var ParserMaterialCategoryDto[] */
    public $materialsUrl = [];
    /** @var string */
    public $clientId;

    /**
     * @param Queue $queue which pushed and is handling the job
     * @return bool
     */
    public function execute($queue): bool
    {
        try {
            $client = Yii::$app->parsers->getClient($this->clientId);
            $serviceName = ucfirst($client->getId());
            $serviceName = 'common\\services\\parsers\\Parser' . $serviceName . 'Service\\' . $serviceName . 'MaterialsService';
            /** @var ParserMaterialsServiceBase $class */
            $class = new $serviceName();
            if (!$class instanceof ParserMaterialsServiceBase) {
                return true;
            }

            $class->setClient($client);
            $class->setMaterialsUrl($this->materialsUrl);
            $class->run();
        } catch (Throwable $exception) {
            Yii::$app->sentry->captureException($exception);
            return true;
        }

        return true;
    }
}
