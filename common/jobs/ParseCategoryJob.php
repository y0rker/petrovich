<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 15:29
 */

namespace common\jobs;

use common\components\Jobs;
use common\services\ParserCategoryServiceBase;
use Throwable;
use Yii;
use yii\queue\JobInterface;
use yii\queue\Queue;

/**
 * Class ParseCategoryJob
 * @package common\jobs
 */
class ParseCategoryJob extends Jobs implements JobInterface
{
    /** @var string */
    public $serviceName;
    /** @var string */
    public $categoryUrl;
    /** @var string */
    public $clientId;

    /**
     * @param Queue $queue
     * @return bool
     */
    public function execute($queue): bool
    {
        try {
            $client = Yii::$app->parsers->getClient($this->clientId);
            $serviceName = ucfirst($client->getId());
            $serviceName = 'common\\services\\parsers\\Parser' . $serviceName . 'Service\\' . $serviceName . 'CategoryService';
            /** @var ParserCategoryServiceBase $class */
            $class = new $serviceName();
            if (!$class instanceof ParserCategoryServiceBase) {
                return true;
            }
            $class->setClient($client);
            $class->setCategoryUrl($this->categoryUrl);
            $class->run();
        } catch (Throwable $exception) {
            Yii::$app->sentry->captureException($exception);
            return true;
        }

        return true;
    }
}
