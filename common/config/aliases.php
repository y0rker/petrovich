<?php

/**
 * Set all application aliases.
 */

Yii::setAlias('api', dirname(__DIR__, 2) . '/api');
Yii::setAlias('console', dirname(__DIR__, 2) . '/console');
Yii::setAlias('common', dirname(__DIR__, 2) . '/common');
Yii::setAlias('logs', dirname(__DIR__, 2) . '/logs');
Yii::setAlias('root', dirname(__DIR__, 2));