<?php

use api\components\ApiClient;
use common\components\ParsersComponent;
use common\components\queue\amqp\Command;
use common\components\queue\amqp\Queue;
use yii\queue\LogBehavior;
use yii\queue\serializers\JsonSerializer;

$queue = [
    'driver'       => \yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_BUNNY,
    'class'        => Queue::class,
    'serializer'   => JsonSerializer::class,
    'commandClass' => Command::class,
    'host'         => 'localhost',
    'heartbeat'    => 1200,
    'port'         => 5672,
    'user'         => 'guest',
    'password'     => 'guest',
    'ttr'          => 10 * 90,
    'debug'        => true,
    'as log'       => LogBehavior::class,
];

$config = [
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'language'   => 'ru-RU',
    'bootstrap'  => [
        'catalog',
    ],
    'components' => [
        'parsers' => [
            'class'   => ParsersComponent::class,
            'clients' => [
                'santechlux' => [
                    'class'      => ApiClient::class,
                    'baseUrl'    => 'http://www.santech-lux.ru',
                    'name'       => 'Artemix',
                    'categories' => [
                        'https://www.santech-lux.ru/1451-zerkala-zerkalnye-shkafy.html'
                    ],
                    'delay'      => 500000
                ],
            ]
        ],
        'sentry'  => [
            'class'       => common\components\sentry\Component::class,
            'dsn'         => 'https://f589749e47e744af8e37a85ff8ee5e49@sentry.ugol.click/8',
            'client'      => [
                'curl_method' => 'async',
            ],
            'environment' => 'production',
        ],
        'catalog' => array_merge($queue, [
            'queueName'    => 'queue-catalog',
            'exchangeName' => 'exchange-catalog',
        ]),
    ],
];

return $config;
