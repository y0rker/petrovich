<?php
/**
 * User: y0rke
 * Date: 08.02.2019
 * Time: 23:53
 */

namespace common\models;

use common\models\base\MaterialBase;
use yii\db\ActiveQuery;

/**
 * Class Material
 *
 * @package common\models
 */
class Material extends MaterialBase
{
    /**
     * @inheritdoc
     * @return ActiveQuery the active query used by this AR class.
     */
    public static function find(): ActiveQuery
    {
        return new ActiveQuery(static::class);
    }
}
