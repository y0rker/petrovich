<?php
/**
 * User: y0rke
 * Date: 08.02.2019
 * Time: 23:56
 */

namespace common\models;

use common\models\base\MaterialPriceBase;
use yii\db\ActiveQuery;

/**
 * Class MaterialPrice
 *
 * @package common\models
 */
class MaterialPrice extends MaterialPriceBase
{
    /**
     * @inheritdoc
     * @return ActiveQuery the active query used by this AR class.
     */
    public static function find(): ActiveQuery
    {
        return new ActiveQuery(static::class);
    }
}
