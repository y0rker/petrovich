<?php

namespace common\models\base;


/**
 * This is the model class for table "material_price".
 *
 * @property integer $id
 * @property integer $material_id
 * @property string  $price
 * @property string  $created
 * @property string  $unit [varchar(255)]
 * @property string  $datestamp [date]
 */
class MaterialPriceBase extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_id', 'price'], 'required'],
            [['material_id'], 'default', 'value' => null],
            [['material_id'], 'integer'],
            [['unit'], 'string', 'max' => 255],
            [['price'], 'number'],
            [['created'], 'safe'],
        ];
    }
}
