<?php

namespace common\models\base;


/**
 * This is the model class for table "parser".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $code
 * @property string  $link
 * @property boolean $visible
 */
class ParserBase extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'link'], 'required'],
            [['visible'], 'boolean'],
            [['name', 'code', 'link'], 'string', 'max' => 255],
        ];
    }

}
