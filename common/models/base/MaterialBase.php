<?php

namespace common\models\base;

use common\models\Category;
use common\models\Parser;

/**
 * This is the model class for table "material".
 *
 * @property integer  $id
 * @property integer  $external_key
 * @property string   $name
 * @property string   $preview
 * @property string   $created
 * @property string   $updated
 * @property integer  $category_id
 * @property string   $link
 * @property string   $vendor
 * @property string   $article
 * @property integer  $parser_id
 * @property string   $description
 *
 * @property Category $category
 * @property Parser   $parser
 * @property string   $images [jsonb]
 */
class MaterialBase extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['external_key', 'name', 'category_id', 'link', 'parser_id'], 'required'],
            [['external_key', 'category_id', 'parser_id'], 'default', 'value' => null],
            [['external_key', 'category_id', 'parser_id'], 'integer'],
            [['created', 'images', 'updated',], 'safe'],
            [['name', 'link', 'vendor', 'article'], 'string', 'max' => 255],
            [['preview'], 'string', 'max' => 500],
            [
                ['category_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Category::class,
                'targetAttribute' => ['category_id' => 'id']
            ],
            [
                ['parser_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Parser::class,
                'targetAttribute' => ['parser_id' => 'id']
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParser()
    {
        return $this->hasOne(Parser::class, ['id' => 'parser_id']);
    }
}
