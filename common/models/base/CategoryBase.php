<?php

namespace common\models\base;

use common\components\ActiveRecord;
use common\models\Parser;
use common\models\Material;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "category".
 *
 * @property integer    $id
 * @property string     $created
 * @property string     $link
 * @property integer    $parser_id
 *
 * @property Parser     $parser
 * @property Material[] $materials
 */
class CategoryBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['link', 'parser_id'], 'required'],
            [['parser_id'], 'default', 'value' => null],
            [['parser_id'], 'integer'],
            [['created'], 'safe'],
            [['link'], 'string', 'max' => 255],
            [
                ['parser_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Parser::class,
                'targetAttribute' => ['parser_id' => 'id']
            ],
        ];
    }


    /**
     * @return ActiveQuery
     */
    public function getParser(): ActiveQuery
    {
        return $this->hasOne(Parser::class, ['id' => 'parser_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMaterials(): ActiveQuery
    {
        return $this->hasMany(Material::class, ['category_id' => 'id']);
    }
}
