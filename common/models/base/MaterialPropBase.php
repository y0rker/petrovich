<?php

namespace common\models\base;

use common\models\Material;

/**
 * This is the model class for table "material_prop".
 *
 * @property integer  $id
 * @property integer  $material_id
 * @property string   $name
 * @property string   $value
 * @property string   $old_value
 * @property string   $created
 * @property string   $updated
 *
 * @property Material $material
 */
class MaterialPropBase extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_prop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_id', 'name', 'value'], 'required'],
            [['material_id'], 'default', 'value' => null],
            [['material_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name', 'value', 'old_value'], 'string', 'max' => 255],
            [['material_id', 'name'], 'unique', 'targetAttribute' => ['material_id', 'name']],
            [
                ['material_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Material::class,
                'targetAttribute' => ['material_id' => 'id']
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::class, ['id' => 'material_id']);
    }
}
