<?php
/**
 * User: y0rke
 * Date: 09.02.2019
 * Time: 11:15
 */

namespace common\models;

use common\models\base\CategoryBase;
use yii\db\ActiveQuery;

/**
 * Class Category
 *
 * @package common\models
 */
class Category extends CategoryBase
{
    /**
     * @inheritdoc
     * @return ActiveQuery the active query used by this AR class.
     */
    public static function find(): ActiveQuery
    {
        return new ActiveQuery(static::class);
    }
}
