<?php

namespace common\components;

use common\components\exceptions\ValidationException;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Exception;

/**
 * Class Model
 *
 * @package common\components
 */
class Model extends \yii\base\Model
{
    /**
     * Returns an instance of the $class with loaded attributes and scenario.
     *
     * @param array  $attributes Attributes to be loaded to the form.
     * @param string $scenario   Scenario to be loaded to the form.
     * @param array  $config     Configuration data, which will be passed to the form constructor
     *
     * @return static
     */
    public static function make(array $attributes, $scenario = null, array $config = [])
    {
        /** @var Model $model Late static binding */
        $model = new static($config);
        $model->setScenario($scenario ?? $model::SCENARIO_DEFAULT);
        if (!$model->load($attributes)) {
            $model->setAttributes($attributes);
        }

        return $model;
    }

    /** @inheritdoc */
    public function getAttributeLabel($attribute): string
    {
        return Yii::t(
            'app',
            parent::getAttributeLabel($attribute)
        );
    }

    /**
     * Wraps standard validate() method with ModelValidationException in case of false.
     *
     * @param array|null $attributeNames
     * @param bool       $clearErrors
     *
     * @return void
     * @throws InvalidParamException
     * @throws ValidationException
     * @throws Exception
     */
    public function validateOrDie(array $attributeNames = null, bool $clearErrors = true): void
    {
        /** @var Model|ActiveRecord $this */
        if (!$this->validate($attributeNames, $clearErrors)) {
            throw new ValidationException($this);
        }
    }
}
