<?php

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use <?= '\\' . $generator->ns . '\\base\\' . $className . 'Base;' . "\n" ?>

/**
* Class <?= $className . PHP_EOL ?>
*
* @package <?= $generator->ns . PHP_EOL ?>
*/
class <?= $className ?> extends <?= $className . 'Base' . "\n" ?>
{
<?php if ($queryClassName): ?>
    <?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    echo "\n";
    ?>
    /**
    * @inheritdoc
    * @return <?= $queryClassFullName ?> the active query used by this AR class.
    */
    public static function find()
    {
    return new <?= $queryClassFullName ?>(static::class);
    }
<?php endif; ?>
}
