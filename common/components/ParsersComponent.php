<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 14:28
 */

namespace common\components;

use api\interfaces\ParserInterface;
use function is_object;
use Yii;
use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;

/**
 * Class ParsersComponent
 * @package common\components
 */
class ParsersComponent extends Component
{
    /**
     * @var array list of parsers clients with their configuration in format: '
     * [
     *  'class' => common\components\ParserClient::class,
     *  'baseUrl' => 'http://www.santech-lux.ru',
     *  'categories' => [
     *      'https://www.santech-lux.ru/186-mebel-dlya-vannoy.html',
     *      'https://www.santech-lux.ru/1451-zerkala-zerkalnye-shkafy.html'
     *  ]
     * ]
     */
    private $_clients = [];

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getClients(): array
    {
        $clients = [];
        foreach ($this->_clients as $id => $client) {
            $clients[$id] = $this->getClient($id);
        }

        return $clients;
    }

    /**
     * @param array $clients list of auth clients
     */
    public function setClients(array $clients): void
    {
        $this->_clients = $clients;
    }

    /**
     * @param string $id
     * @return ParserInterface
     * @throws InvalidConfigException
     */
    public function getClient(string $id): ParserInterface
    {
        if (!array_key_exists($id, $this->_clients)) {
            throw new InvalidArgumentException("Unknown auth client '{$id}'.");
        }
        if (!is_object($this->_clients[$id])) {
            $this->_clients[$id] = $this->createClient($id, $this->_clients[$id]);
        }

        return $this->_clients[$id];
    }

    /**
     * @param $id
     * @param $config
     * @return ParserInterface
     * @throws InvalidConfigException
     */
    protected function createClient($id, $config): ParserInterface
    {
        $config['id'] = $id;

        /** @var ParserInterface $object */
        $object = Yii::createObject($config);

        return $object;
    }
}
