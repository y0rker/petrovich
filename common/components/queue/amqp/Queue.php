<?php

namespace common\components\queue\amqp;

use function call_user_func;
use common\components\ActiveRecord;
use common\components\log\Log;
use Enqueue\AmqpBunny\AmqpConsumer;
use Exception;
use Interop\Amqp\AmqpQueue;
use Interop\Amqp\AmqpTopic;
use Interop\Amqp\Impl\AmqpBind;
use Interop\Amqp\Impl\AmqpMessage;
use Throwable;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\helpers\VarDumper;
use yii\queue\ExecEvent;
use yii\queue\JobInterface;
use yii\queue\PushEvent;
use yii\queue\RetryableJobInterface;

/**
 * Amqp Queue
 *
 * @author Roman Zhuravlev <zhuravljov@gmail.com>
 */
class Queue extends \yii\queue\amqp_interop\Queue
{
    public const TTR = 600;

    public $maxPriority = false;
    public $debug = false;

    public $pushTtr;
    public $pushDelay;
    public $pushPriority;

    /**
     * Listens amqp-queue and runs new jobs.
     */
    public function listen()
    {
        $this->open();
        $this->setupBroker();

        $queue    = $this->context->createQueue($this->queueName);
        $consumer = $this->context->createConsumer($queue);
        $this->context->subscribe($consumer, function (AmqpMessage $message, AmqpConsumer $consumer) {
            if ($message->isRedelivered()) {
                $consumer->acknowledge($message);

                $this->redeliver($message);

                return true;
            }

            $ttr     = $message->getProperty(self::TTR);
            $attempt = $message->getProperty(self::ATTEMPT, 1);

            if ($this->handleMessage($message->getMessageId(), $message->getBody(), $ttr, $attempt)) {
                $consumer->acknowledge($message);
            } else {
                $consumer->acknowledge($message);

                $this->redeliver($message);
            }

            return true;
        });

        $this->context->consume();
    }

    /**
     * @param string|null          $id
     * @param JobInterface         $job
     * @param int                  $ttr
     * @param int                  $attempt
     * @param Exception|Throwable $error
     *
     * @return bool
     * @throws \yii\db\Exception
     * @internal
     */
    public function handleError($id, $job, $ttr, $attempt, $error): bool
    {
        $data = [
            'id'      => $id,
            'job'     => $job,
            'ttr'     => $ttr,
            'attempt' => $attempt,
            'error'   => $error,
            'retry'   => $job instanceof RetryableJobInterface
                ? $job->canRetry($attempt, $error)
                : $attempt < $this->attempts,
        ];

        ActiveRecord::rollbackActiveTransaction();

        Log::error('Ошибка в очереди', ['event' => print_r($data, true)]);

        return parent::handleError($id, $job, $ttr, $attempt, $error);
    }

    protected function setupBroker()
    {
        if ($this->setupBrokerDone) {
            return;
        }

        $queue = $this->context->createQueue($this->queueName);
        $queue->addFlag(AmqpQueue::FLAG_DURABLE);
        if ($this->maxPriority) {
            $queue->setArguments(['x-max-priority' => $this->maxPriority]);
        }
        $this->context->declareQueue($queue);

        $topic = $this->context->createTopic($this->exchangeName);
        $topic->setType(AmqpTopic::TYPE_DIRECT);
        $topic->addFlag(AmqpTopic::FLAG_DURABLE);
        $this->context->declareTopic($topic);

        $this->context->bind(new AmqpBind($queue, $topic));

        $this->setupBrokerDone = true;
    }

    /**
     * @param string $id
     * @param string $message
     * @param int    $ttr
     * @param int    $attempt
     *
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws \yii\db\Exception
     */
    protected function handleMessage($id, $message, $ttr, $attempt)
    {
        if (!$this->debug && $this->messageHandler) {
            return call_user_func($this->messageHandler, $id, $message, $ttr, $attempt);
        }

        return $this->messageHandler($id, $message, $ttr, $attempt);
    }

    /**
     * @param string $id      of a job message
     * @param string $message
     * @param int    $ttr     time to reserve
     * @param int    $attempt number
     *
     * @return bool
     * @throws InvalidArgumentException
     * @throws \yii\db\Exception
     */
    protected function messageHandler($id, $message, $ttr, $attempt): bool
    {
        $job = $this->serializer->unserialize($message);
        if (!($job instanceof JobInterface)) {
            $dump = VarDumper::dumpAsString($job);
            throw new InvalidArgumentException("Job $id must be a JobInterface instance instead of $dump.");
        }

        $event = new ExecEvent([
            'id'      => $id,
            'job'     => $job,
            'ttr'     => $ttr,
            'attempt' => $attempt,
        ]);
        $this->trigger(self::EVENT_BEFORE_EXEC, $event);
        if ($event->handled) {
            return true;
        }

        try {
            $event->job->execute($this);
        } catch (Exception $error) {
            return $this->handleError($event->id, $event->job, $event->ttr, $event->attempt, $error);
        } catch (Throwable $error) {
            return $this->handleError($event->id, $event->job, $event->ttr, $event->attempt, $error);
        }
        $this->trigger(self::EVENT_AFTER_EXEC, $event);

        return true;
    }

    /**
     * Pushes job into queue
     *
     * @param JobInterface|mixed $job
     *
     * @return string|null id of a job message
     */
    public function push($job)
    {
        $event              = new PushEvent([
            'job'      => $job,
            'ttr'      => $job instanceof RetryableJobInterface ? $job->getTtr() : ($this->pushTtr ?: $this->ttr),
            'delay'    => $this->pushDelay ?: 0,
            'priority' => $this->pushPriority,
        ]);
        $this->pushTtr      = null;
        $this->pushDelay    = null;
        $this->pushPriority = null;

        $this->trigger(self::EVENT_BEFORE_PUSH, $event);
        if ($event->handled) {
            return null;
        }

        if ($this->strictJobType && !($event->job instanceof JobInterface)) {
            throw new InvalidParamException('Job must be instance of JobInterface.');
        }

        $message   = $this->serializer->serialize($event->job);
        $event->id = $this->pushMessage($message, $event->ttr, $event->delay, $event->priority);
        $this->trigger(self::EVENT_AFTER_PUSH, $event);

        return $event->id;
    }

    /**
     * @inheritdoc
     * @param $payload
     * @param $ttr
     * @param $delay
     * @param $priority
     * @return
     */
    protected function pushMessage($payload, $ttr, $delay, $priority)
    {
        $this->open();
        $this->setupBroker();

        $topic = $this->context->createTopic($this->exchangeName);

        $message = $this->context->createMessage($payload);
        $message->setDeliveryMode(\Interop\Amqp\AmqpMessage::DELIVERY_MODE_PERSISTENT);
        $message->setMessageId(uniqid('', true));
        $message->setTimestamp(time());
        $message->setProperty(self::ATTEMPT, 1);
        $message->setProperty(self::TTR, $ttr);

        $producer = $this->context->createProducer();

        if ($delay) {
            $message->setProperty(self::DELAY, $delay);
            $producer->setDeliveryDelay($delay * 1000);
        }

        if ($priority) {
            $message->setProperty(self::PRIORITY, $priority);
            $producer->setPriority($priority);
        }

        $producer->send($topic, $message);

        return $message->getMessageId();
    }
}
