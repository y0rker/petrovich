<?php

namespace common\components\queue\amqp;

use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Symfony\Component\Process\Process;

/**
 * Class Command
 *
 * @package common\components\queue\amqp
 */
class Command extends \yii\queue\amqp_interop\Command
{
    public $blackfire = false;

    /**
     * Handles message using child process.
     *
     * @param string|null $id      of a message
     * @param string      $message
     * @param int         $ttr     time to reserve
     * @param int         $attempt number
     *
     * @return bool
     * @throws
     * @see actionExec()
     */
    protected function handleMessage($id, $message, $ttr, $attempt)
    {
        // Executes child process
        $cmd = strtr('{blackfire}php yii queue/exec "id" "ttr" "attempt" "pid"', [
            '{blackfire}' => $this->blackfire ? 'blackfire run ' : '',
            'php'         => PHP_BINARY,
            'yii'         => $_SERVER['SCRIPT_FILENAME'],
            'queue'       => $this->uniqueId,
            'id'          => $id,
            'ttr'         => $ttr,
            'attempt'     => $attempt,
            'pid'         => $this->queue->getWorkerPid(),
        ]);
        foreach ($this->getPassedOptions() as $name) {
            if (\in_array($name, $this->options('exec'), true)) {
                $cmd .= ' --' . $name . '=' . $this->$name;
            }
        }
        if (!\in_array('color', $this->getPassedOptions(), true)) {
            $cmd .= ' --color=' . $this->isColorEnabled();
        }

        $cwd = \dirname($_SERVER['SCRIPT_FILENAME']);

        $process = new Process($cmd, $cwd, null, $message, $ttr);
        try {
            $process->run(function ($type, $buffer) {
                if ($type === Process::ERR) {
                    $this->stderr($buffer);
                } else {
                    $this->stdout($buffer);
                }
            });
        } catch (ProcessTimedOutException $error) {
            $job = $this->queue->serializer->unserialize($message);

            return $this->queue->handleError($id, $job, $ttr, $attempt, $error);
        }

        return $process->isSuccessful();
    }
}
