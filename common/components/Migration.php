<?php
/**
 * Created by PhpStorm.
 * Module: andkon
 * Date: 22.03.17
 * Time: 12:30
 */

namespace common\components;

/**
 * Class Migration
 *
 * @package console\components
 */
class Migration extends \andkon\migrate\Migration
{
    public const RULE_CASCADE = 'CASCADE';
    public const RULE_NO_ACTION = 'NO ACTION';
    protected $tableOptions = '';

    /**
     * @return \yii\db\ColumnSchemaBuilder
     */
    protected function jsonb()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('jsonb');
    }

    /**
     * @param int $precision
     * @param int $scale
     *
     * @return \yii\db\ColumnSchemaBuilder
     */
    protected function price($precision = 10, $scale = 2)
    {
        return $this->decimal($precision, $scale);
    }

    /**
     * @param int $precision
     * @param int $scale
     *
     * @return \yii\db\ColumnSchemaBuilder
     */
    protected function dec($precision = 12, $scale = 4)
    {
        return $this->decimal($precision, $scale);
    }
}
