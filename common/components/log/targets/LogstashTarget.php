<?php

namespace common\components\log\targets;

use function call_user_func;
use common\components\log\Logger;
use Exception;
use function is_array;
use function is_callable;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\web\User;

/**
 * Class LogstashTarget
 *
 * @package common\components\log\targets
 * @property array $contextMessage
 */
class LogstashTarget extends \index0h\log\LogstashTarget
{
    public $logVars = ['_GET', '_POST', '_FILES'];
    public $includeGlobals = false;
    public $includeTraces = false;
    public $timeout = 2;
    public $flags = STREAM_CLIENT_CONNECT | STREAM_CLIENT_ASYNC_CONNECT;

    /**
     * @inheritdoc
     */
    public function export()
    {
        try {
            $socket = stream_socket_client($this->dsn, $errorNumber, $error, $this->timeout, $this->flags);
            stream_set_blocking($socket, 0);

            foreach ($this->messages as &$message) {
                fwrite($socket, $this->formatMessage($message) . "\n");
            }
            unset($message);

            fclose($socket);
        } catch (Exception $error) {
            $this->emergencyExport(
                [
                    'dsn'         => $this->dsn,
                    'error'       => $error->getMessage(),
                    'errorNumber' => $error->getCode(),
                    'trace'       => $error->getTraceAsString()
                ]
            );
        }
    }

    /**
     * Transform log message to assoc.
     *
     * @param array $message The log message.
     *
     * @return array
     */
    protected function prepareMessage($message): array
    {
        [$text, $level, $category, $timestamp] = $message;

        $level     = Logger::getLevelName($level);
        $timestamp = date('c', $timestamp);

        if (!empty($text['extra'])) {
            if (is_array($text['extra'])) {
                foreach ($text['extra'] as $key => $value) {
                    if (is_array($value) || is_object($value)) {
                        $value = json_encode($value, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    }
                    $text[$key] = (string)$value;
                }
            } elseif (is_callable($text['extra'])) {
                $cb = call_user_func($text['extra']);
                if (is_array($cb)|| is_object($cb)) {
                    $cb = json_encode($cb, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                }
                $text['callable'] = (string)$cb;
            }

            unset($text['extra']);
        }

        $result = ArrayHelper::merge(
            $text,
            ['level' => $level, 'category' => $category, '@timestamp' => $timestamp]
        );

        if ($this->includeTraces && isset($message[4]) === true) {
            $result['trace'] = $message[4];
        }

        return $result;
    }

    /**
     * Generates the context information to be logged.
     *
     * @return array
     * @throws InvalidConfigException
     */
    protected function getContextMessage(): array
    {
        $context = $this->context;

        if (($this->logUser === true) && ($user = Yii::$app->get('user', false)) !== null) {
            /** @var User $user */
            $context['context']['userId'] = (string)$user->getId();
        }

        if (!$this->includeGlobals) {
            return $context;
        }

        foreach ($this->logVars as $name) {
            if (empty($GLOBALS[$name]) === false) {
                $context['global'][$name] = json_encode(
                    $GLOBALS[$name],
                    JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                );
            }
        }

        return $context;
    }
}
