<?php

namespace common\components\log;

/**
 * Class Logger
 *
 * @package common\components\log
 */
class Logger extends \yii\log\Logger
{
    public const CATALOG = 'catalog';
}
