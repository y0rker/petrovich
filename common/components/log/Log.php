<?php

namespace common\components\log;

use function is_array;
use Yii;

/**
 * Class Log
 *
 * @package common\components\log
 */
class Log
{
    /**
     * @param string         $message
     * @param array|callable $extra
     * @param string         $category
     * @param string         $category_sub
     */
    public static function error(string $message, $extra = [], $category = 'app', $category_sub = 'default'): void
    {
        if (is_array($extra)) {
            $extra['category_sub'] = $category_sub;
        }
        $msg = [
            '@message' => $message,
            'extra'    => $extra,
        ];
        Yii::error($msg, $category);
    }

    /**
     * @param string         $message
     * @param array|callable $extra
     * @param string         $category
     * @param string         $category_sub
     */
    public static function warning(string $message, $extra = [], $category = 'app', $category_sub = 'default'): void
    {
        if (is_array($extra)) {
            $extra['category_sub'] = $category_sub;
        }
        $msg = [
            '@message' => $message,
            'extra'    => $extra,
        ];
        Yii::warning($msg, $category);
    }

    /**
     * @param string         $message
     * @param array|callable $extra
     * @param string         $category
     * @param string         $category_sub
     */
    public static function info(string $message, $extra = [], $category = 'app', $category_sub = 'default'): void
    {
        if (is_array($extra)) {
            $extra['category_sub'] = $category_sub;
        }
        $msg = [
            '@message' => $message,
            'extra'    => $extra,
        ];
        Yii::info($msg, $category);
    }
}
