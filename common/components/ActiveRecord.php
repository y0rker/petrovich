<?php

namespace common\components;

use common\components\exceptions\ValidationException;
use function is_callable;
use yii;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\AfterSaveEvent;
use yii\db\Exception;

/**
 * Class ActiveRecord
 *
 * @package common
 */
class ActiveRecord extends yii\db\ActiveRecord
{
    protected $isNeedUpdated = false;

    /**
     * @param array $columns
     *
     * @return array
     */
    public static function tableColumns(array $columns): array
    {
        $arr = [];
        foreach ($columns as $column) {
            $arr[] = static::tableColumn($column);
        }

        return $arr;
    }

    /**
     * @param string $column
     *
     * @return string
     */
    public static function tableColumn(string $column): string
    {
        return static::tableName() . '.' . $column;
    }

    /**
     * @param array $column
     * @param array $data
     *
     * @return int
     * @throws Exception
     */
    public static function addBatch(array $column, array $data): int
    {
        return \Yii::$app->getDb()
            ->createCommand()
            ->batchInsert(static::tableName(), $column, [$data])->execute();
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public static function find()
    {
        return Yii::createObject(ActiveQuery::class, [static::class]);
    }

    /**
     * Ищет модель или Создает модель с переданными атрибутами
     *
     * @param array $attributes
     *
     * @return static
     */
    public static function findOrCreate($attributes)
    {
        $model = self::findOrInit($attributes);
        if ($model->getIsNewRecord()) {
            $model->save();
        }

        return $model;
    }

    /**
     * Ищет модель по атрибутам или инициализирует новую с переданными атрибутами
     *
     * @param array $attributes
     *
     * @return static
     */
    public static function findOrInit($attributes)
    {
        /** @var \yii\db\ActiveRecord $class */
        $class = static::class;
        /** @var static $model */
        $model = $class::findOne($attributes);
        if (!$model) {
            $model = new $class();
            $model->setAttributes($attributes);
        }

        return $model;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @param bool $skipIfSet
     *
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null, bool $skipIfSet = true): bool
    {
        $this->loadDefaultValues($skipIfSet);

        return parent::save($runValidation, $attributeNames);
    }

    /**
     * Ищет модель или убивает
     *
     * @param array    $attributes
     * @param callable $callback
     *
     * @return static
     * @throws yii\web\NotFoundHttpException
     * @throws Exception
     */
    public static function findOrDie($attributes, ?callable $callback = null)
    {
        /** @var \yii\db\ActiveRecord $class */
        $class = static::class;
        /** @var static $model */
        $model = $class::findOne($attributes);
        if (!$model) {
            self::rollbackActiveTransaction();
            if (is_callable($callback)) {
                $callback(null);
            }
            throw new yii\web\NotFoundHttpException();
        }

        if (is_callable($callback)) {
            $callback($model);
        }

        return $model;
    }

    /**
     * @throws Exception
     */
    public static function rollbackActiveTransaction(): void
    {
        $transaction = \Yii::$app->getDb()->getTransaction();
        if ($transaction) {
            $transaction->rollBack();
        }
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function loadFromForm(Model $data): bool
    {
        return $this->load($data->attributes, '');
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        $fieldNames = ['name', 'title'];
        foreach ($fieldNames as $fieldName) {
            if ($this->hasAttribute($fieldName)) {
                return (string)$this->getAttribute($fieldName);
            }
        }

        return 'Объект класса ' . static::class;
    }

    /**
     * Load default values from DB scheme and save model
     *
     * @param bool       $runValidation
     * @param null|array $attributeNames
     * @param bool       $skipIfSet
     *
     * @return bool
     */
    public function loadDefaultAndSave(bool $runValidation = true, $attributeNames = null, bool $skipIfSet = true): bool
    {
        return $this->loadDefaultValues($skipIfSet)->save($runValidation, $attributeNames);
    }

    /**
     * @inheritdoc
     * @return bool
     * @throws InvalidParamException
     */
    public function beforeValidate(): bool
    {
        if (!strpos(static::class, 'Search')) {
            if ($this->getIsNewRecord()) {
                if ($this->hasAttribute('created')) {
                    $this->setAttribute('created', date('Y-m-d H:i:s'));
                }
                if ($this->isNeedUpdated && $this->hasAttribute('updated')) {
                    $this->setAttribute('updated', date('Y-m-d H:i:s'));
                }
            } else if ($this->hasAttribute('updated')) {
                $this->setAttribute('updated', date('Y-m-d H:i:s'));
            }
        }

        return parent::beforeValidate();
    }

    /** @inheritdoc */
    public function getAttributeLabel($attribute): string
    {
        return Yii::t(
            'app',
            parent::getAttributeLabel($attribute)
        );
    }

    /**
     *  EventManager::EVENT_PROVIDER_UPDATE => [
     *      handler::class => [
     *          'on'     => [],
     *          'except' => [],
     *      ]
     *  ],
     *
     * @return array
     */
    public function event(): array
    {
        return [];
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @param bool $skipIfSet
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function saveOrDie($runValidation = true, $attributeNames = null, bool $skipIfSet = true): void
    {
        if (!$this->save($runValidation, $attributeNames, $skipIfSet)) {
            $transaction = \Yii::$app->getDb()->getTransaction();
            if ($transaction) {
                $transaction->rollBack();
            }
            throw new ValidationException($this);
        }
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->trigger(self::EVENT_INIT, new Event());
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->trigger(self::EVENT_AFTER_FIND, new Event());
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     *
     * @throws InvalidConfigException
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->trigger($insert ? self::EVENT_AFTER_INSERT : self::EVENT_AFTER_UPDATE, new AfterSaveEvent([
            'changedAttributes' => $changedAttributes,
        ]));

        $eventName = $this->getEventFormName(($insert ? self::EVENT_AFTER_INSERT : self::EVENT_AFTER_UPDATE));
        Yii::$app->trigger($eventName, new AfterSaveEvent([
            'changedAttributes' => $changedAttributes,
            'sender'            => $this,
        ]));
    }

    /**
     * @param string $event
     *
     * @return string
     * @throws InvalidConfigException
     */
    protected function getEventFormName(string $event): string
    {
        return $event . '_' . $this->formName();
    }

    /**
     * @throws InvalidConfigException
     */
    public function afterDelete()
    {
        $this->trigger(self::EVENT_AFTER_DELETE, new Event());

        $eventName = $this->getEventFormName(self::EVENT_AFTER_DELETE);

        Yii::$app->trigger($eventName, new Event([
            'sender' => $this,
        ]));
    }

    /**
     * @inheritdoc
     */
    public function afterRefresh()
    {
        $this->trigger(self::EVENT_AFTER_REFRESH, new Event());
    }
}
