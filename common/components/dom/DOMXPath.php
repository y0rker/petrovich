<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 17:02
 */

namespace common\components\dom;

use DOMElement;
use DOMNode;
use DOMNodeList;

/**
 * Class DOMXPath
 * @package common\components\dom
 */
class DOMXPath extends \DOMXPath
{
    /**
     * @param string        $expression
     * @param DOMNode|null $contextnode
     * @param bool          $registerNodeNS
     * @return DOMNodeList|false|void|DOMElement[]
     */
    public function query($expression, ?DOMNode $contextnode = null, $registerNodeNS = true)
    {
        return parent::query($expression, $contextnode, $registerNodeNS);
    }
}
