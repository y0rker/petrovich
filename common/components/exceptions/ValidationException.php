<?php

namespace common\components\exceptions;

use common\components\ActiveRecord;
use common\components\Model;
use yii\web\HttpException;

/**
 * Class ValidationException
 *
 * @package api\components\exceptions
 */
class ValidationException extends HttpException
{
    /**
     * Constructor.
     *
     * @param ActiveRecord|Model $model
     *
     * @throws \yii\db\Exception
     */
    public function __construct($model)
    {
        $transaction = \Yii::$app->getDb()->getTransaction();
        if ($transaction) {
            $transaction->rollBack();
        }
        parent::__construct(400, json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE));
    }
}
