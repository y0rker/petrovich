<?php

namespace common\components\db;

use yii\db\Exception;

/**
 * Class Command
 *
 * @package common\components\db
 */
class Command extends \yii\db\Command
{

    /**
     * Executes the SQL statement and returns the first row of the result.
     * This method is best used when only the first row of result is needed for a query.
     *
     * @param int $fetchMode the result fetch mode. Please refer to [PHP
     *                       manual](http://php.net/manual/en/pdostatement.setfetchmode.php) for valid fetch modes. If
     *                       this parameter is null, the value set in [[fetchMode]] will be used.
     *
     * @return array|false the first row (in terms of an array) of the query result. False is returned if the query
     * results in nothing.
     * @throws Exception execution failed
     */
    public function fetchObject($fetchMode = null)
    {
        return $this->queryInternal('fetchObject', $fetchMode);
    }
}
