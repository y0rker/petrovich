<?php

namespace common\components\sentry;

use Closure;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class Component
 * @package common\components\sentry
 */
class Component extends \yii\base\Component
{

    /**
     * Set to `false` in development environment to skip collecting errors
     *
     * @var bool
     */
    public $enabled = true;

    /**
     * @var string Sentry DSN
     * @note this is ignored if [[client]] is a Raven client instance.
     */
    public $dsn;

    /**
     * @var string public Sentry DSN for raven-js
     * If not set, this is generated from the private dsn.
     */
    public $publicDsn;

    /**
     * @var string environment name
     * @note this is ignored if [[client]] is a Raven client instance.
     */
    public $environment = 'production';

    /**
     * collect JavaScript errors
     *
     * @var bool
     */
    public $jsNotifier = false;

    /**
     * Raven-JS configuration array
     *
     * @var array
     * @see https://docs.getsentry.com/hosted/clients/javascript/config/
     */
    public $jsOptions;

    /**
     * @var \Raven_Client|array Raven client or configuration array used to instantiate one
     */
    public $client = [];

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!$this->enabled) {
            return;
        }

        $this->validateDsn();
        $this->setRavenClient();
        $this->setEnvironmentOptions();
        $this->generatePublicDsn();
    }

    /**
     * @throws InvalidConfigException
     */
    private function validateDsn()
    {
        if (empty($this->dsn)) {
            throw new InvalidConfigException('Private DSN must be set!');
        }

        // throws \InvalidArgumentException if dsn is invalid
        \Raven_Client::parseDSN($this->dsn);
    }

    /**
     * @throws InvalidConfigException
     */
    private function setRavenClient()
    {
        if (is_array($this->client)) {
            $ravenClass   = ArrayHelper::remove($this->client, 'class', '\Raven_Client');
            $options      = $this->client;
            $this->client = new $ravenClass($this->dsn, $options);
        } elseif (!is_object($this->client) || $this->client instanceof Closure) {
            $this->client = Yii::createObject($this->client);
        }

        if (!is_object($this->client)) {
            throw new InvalidConfigException(get_class($this) . '::' . 'client must be an object');
        }
    }

    /**
     * Adds a tag to filter events by environment
     */
    private function setEnvironmentOptions()
    {
        if (empty($this->environment)) {
            return;
        }

        if (is_object($this->client) && property_exists($this->client, 'environment')) {
            $this->client->environment = $this->environment;
        }
        $this->jsOptions['environment'] = $this->environment;
    }

    private function generatePublicDsn()
    {
        if ($this->publicDsn === null && $this->jsNotifier === true) {
            $this->publicDsn = preg_replace(
                '/^(https:\/\/|http:\/\/)([a-z0-9]*):([a-z0-9]*)@(.*)/',
                '$1$2@$4',
                $this->dsn
            );
        }
    }

    /**
     * @param       $message
     * @param       $params
     * @param array $levelOrOptions
     * @param bool  $stack
     * @param null  $vars
     *
     * @return string|null
     */
    public function captureMessage($message, $params, $levelOrOptions = [], $stack = false, $vars = null)
    {
        return $this->client->captureMessage($message, $params, $levelOrOptions, $stack, $vars);
    }

    /**
     * @param      $exception
     * @param null $culpritOrOptions
     * @param null $logger
     * @param null $vars
     *
     * @return string|null
     */
    public function captureException($exception, $culpritOrOptions = null, $logger = null, $vars = null)
    {
        return $this->client->captureException($exception, $culpritOrOptions, $logger, $vars);
    }

    /**
     * @param      $data
     * @param null $stack
     * @param null $vars
     *
     * @return |null
     */
    public function capture($data, $stack = null, $vars = null)
    {
        return $this->client->capture($data, $stack, $vars);
    }

    /**
     * @param array $data
     */
    public function setExtra($data)
    {
        $this->client->extra_context((array)$data);
    }
}
