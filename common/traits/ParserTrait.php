<?php
/**
 * User: y0rker
 * Date: 30.05.19
 * Time: 18:11
 */

namespace common\traits;

use common\services\CurlRequestService;
use DOMDocument;
use DOMElement;
use yii\web\ServerErrorHttpException;

/**
 * Class ParserTrait
 * @package common\services\parser\traits
 */
trait ParserTrait
{
    /**
     * @param string $url
     * @return DOMDocument|null
     * @throws ServerErrorHttpException
     */
    protected function getDoc(string $url): ?DOMDocument
    {
        $curlService = new CurlRequestService();
        $html = $curlService->sendGetRequest($url);
        if ($html === false) {
            return null;
        }
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->preserveWhiteSpace = false;
        $doc->loadHTML($html);

        return $doc;
    }

    /**
     * @param $node
     *
     * @return string
     */
    private function getInnerHtml(DOMElement $node): string
    {
        $innerHTML = '';
        $children = $node->childNodes;
        foreach ($children as $child) {
            $innerHTML .= $child->ownerDocument->saveXML($child);
        }

        return $innerHTML;
    }
}
