<?php
/**
 * User: y0rker
 * Date: 17.01.19
 * Time: 17:08
 */

namespace common\services;

use api\interfaces\Service;
use api\services\BaseService;
use yii\web\ServerErrorHttpException;

/**
 * Class CurlRequestService
 * @package app\services
 */
class CurlRequestService extends BaseService implements Service
{
    //todo random UA
    private const UA         = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36';
    private const DEBUG      = false;
    private const TORADDRESS = 'localhost:9050';

    /** @var bool */
    protected $tor = true;
    /** @var int */
    private $trying = 1;

    /**
     * @return mixed
     */
    public function run()
    {

    }

    /**
     * @param       $url
     * @param array $postDATA
     * @param bool  $referer
     * @param array $additionalHeader
     * @return bool|string
     */
    public function sendPostRequest($url, $postDATA = array(), $referer = false, $additionalHeader = array())
    {
        $ch = curl_init();

        if (!$referer) {
            $referer = $url;
        }

        $headers = array(
            'Accept: */*',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
        );

        foreach ($additionalHeader as $headVal) {
            $headers[] = $headVal;
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, self::UA);

        if (self::DEBUG) {
            curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
            curl_setopt($ch, CURLOPT_STDERR, $verbose = fopen('php://temp', 'rwb+'));
            curl_setopt($ch, CURLOPT_FILETIME, TRUE);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postDATA));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, './cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, './cookie.txt');
        curl_setopt($ch, CURLOPT_REFERER, $referer);

        if ($this->tor) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            curl_setopt($ch, CURLOPT_PROXY, self::TORADDRESS);
        }

        $out = curl_exec($ch);

        curl_close($ch);

        return $out;
    }

    /**
     * @param       $url
     * @param int   $delay
     * @param bool  $referer
     * @param array $additionalHeader
     * @return bool|string
     * @throws ServerErrorHttpException
     */
    public function sendGetRequest($url, int $delay = 500000, $referer = false, $additionalHeader = [])
    {
        $ch = curl_init();

        if (!$referer) {
            $referer = $url;
        }

        $headers = array(
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
            //'DNT: 1',
            'Cache-Control: max-age=0',
            'Connection: keep-alive'
        );

        foreach ($additionalHeader as $headVal) {
            $headers[] = $headVal;
        }

        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_ENCODING, '');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, self::UA);

        if (self::DEBUG) {
            curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
            curl_setopt($ch, CURLOPT_STDERR, $verbose = fopen('php://temp', 'rwb+'));
            curl_setopt($ch, CURLOPT_FILETIME, TRUE);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, './cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, './cookie.txt');
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if ($this->tor) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            curl_setopt($ch, CURLOPT_PROXY, self::TORADDRESS);
        }

        $out = curl_exec($ch);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code === 404) {
            return false;
        }
        if ($http_code !== 200) {
            $this->trying++;
            usleep(500000);
            $out = $this->sendGetRequest($url, $delay, $referer, $additionalHeader);
        }

        return $out;
    }

    /**
     * @param bool $tor
     * @return CurlRequestService
     */
    public function setTor(bool $tor): CurlRequestService
    {
        $this->tor = $tor;

        return $this;
    }
}
