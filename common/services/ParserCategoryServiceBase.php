<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 15:33
 */

namespace common\services;

use api\interfaces\ParserInterface;
use api\interfaces\Service;
use api\services\BaseService;
use common\models\Category;
use common\models\Parser;
use common\traits\ParserTrait;

/**
 * Class ParserCategoryServiceBase
 * @package common\services
 */
abstract class ParserCategoryServiceBase extends BaseService implements Service
{
    use ParserTrait;

    /** @var ParserInterface */
    protected $client;
    /** @var string */
    protected $categoryUrl;
    /** @var Parser */
    protected $parser;
    /** @var Category */
    protected $category;

    /**
     * @return void
     */
    public function run(): void
    {
        $this->parser = Parser::findOrCreate([
            'name' => $this->client->getName(),
            'code' => $this->client->getId(),
            'link' => $this->client->getBaseUrl()
        ]);

        $this->category = Category::findOrCreate([
            'link'      => $this->categoryUrl,
            'parser_id' => $this->parser->id
        ]);

        $this->parse();
    }

    /**
     * @return void
     */
    abstract protected function parse(): void;

    /**
     * @param ParserInterface $client
     * @return ParserCategoryServiceBase
     */
    public function setClient(ParserInterface $client): ParserCategoryServiceBase
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @param string $category
     * @return ParserCategoryServiceBase
     */
    public function setCategoryUrl(string $category): ParserCategoryServiceBase
    {
        $this->categoryUrl = $category;

        return $this;
    }
}
