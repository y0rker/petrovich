<?php
/**
 * User: y0rker
 * Date: 12.12.18
 * Time: 18:44
 */

namespace common\services;

use api\components\ApiClient;
use api\interfaces\ParserInterface;
use api\interfaces\Service;
use api\services\BaseService;
use common\jobs\ParseCategoryJob;
use Yii;

/**
 * Class ParserServiceBase
 * @package common\services
 */
abstract class ParserServiceBase extends BaseService implements Service
{
    /** @var string */
    protected $url;
    /** @var ApiClient */
    protected $client;
    /** @var */
    protected $logFile;
    /** @var bool */
    protected $forceUpdate = false;

    /**
     * @return boolean
     */
    public function run(): bool
    {
        foreach ($this->client->getCategories() ?? [] as $categoryUrl) {
            $job = new ParseCategoryJob([
                'categoryUrl' => $categoryUrl,
                'clientId'    => $this->client->getId()
            ]);
            Yii::$app->catalog->push($job);
        }

        return true;
    }

    /**
     * @param ParserInterface $client
     * @return self
     */
    public function setClient(ParserInterface $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @param bool $forceUpdate
     * @return ParserServiceBase
     */
    public function setForceUpdate(bool $forceUpdate): ParserServiceBase
    {
        $this->forceUpdate = $forceUpdate;

        return $this;
    }
}
