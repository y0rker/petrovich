<?php
/**
 * User: y0rker
 * Date: 30.05.19
 * Time: 18:16
 */

namespace common\services\parsers\PetrovichService;


use api\interfaces\Service;
use api\services\BaseService;
use common\dto\MaterialDto;
use common\helpers\ParserHelper;
use common\models\Category;
use common\models\Parser;
use common\services\parsers\PetrovichService\traits\PetrovichTrait;
use common\services\parsers\SaveMaterials;
use common\services\parsers\traits\ParserTrait;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class PetrovichCatalogService
 * @package common\services\parser\PetrovichService
 */
class PetrovichCatalogService extends BaseService implements Service
{
    use PetrovichTrait;
    use ParserTrait;

    /** @var MaterialDto[] */
    private $materials = [];
    /** @var Parser */
    private $parser;

    /**
     * PetrovichCatalogService constructor.
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function __construct()
    {
        $this->parser = Parser::findOrDie([
            'code'  => PetrovichService::CODE_SERVICE
        ]);
    }

    /**
     * @return mixed|void
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function run()
    {
        $categories = Category::find()
            ->select(['id', 'name', 'external_key'])
            ->where([
                'parser_id' => $this->parser->id
            ])
            ->all();
        $i = 1;
        foreach ($categories as $category) {
            $url = ParserHelper::CATALOG_URL . $category->external_key;
            echo 'Категория №' . $i . PHP_EOL;
            $this->materials = [];
            $this->parseUrl($url, $category);

            SaveMaterials::getInstance()
                ->setMaterialsDto($this->materials)
                ->run();

            $i++;
        }
    }

    /**
     * @param string   $url
     * @param Category $category
     * @param bool     $recursive
     *
     * @return bool
     * @throws Exception
     */
    private function parseUrl(string $url, Category $category, bool $recursive = true): bool
    {
        $doc = $this->getDoc($url);
        $xpath = new DOMXPath($doc);

        $materials = $xpath->query(ParserHelper::MATERIAL_SELECTOR);
        foreach ($materials as $material) {
            $dto = $this->getDto($material);
            if ($dto === null) {
                continue;
            }
            if (!empty($this->materials[$dto->external_key])) {
                return true;
            }

            $this->materials[$dto->external_key] = $dto;
        }

        if ($recursive) {
            $i = 1;
            do {
                $break = $this->parseUrl($url . '?p=' . $i, $category, false);
                echo 'url: ' . $url . '?p=' . $i . PHP_EOL;
                if ($break) {
                    break;
                }
                $i++;
            } while (true);
        }

        return false;
    }

    /**
     * @param $material
     *
     * @return MaterialDto|null
     */
    private function getDto(DOMElement $material): ?MaterialDto
    {
        $dto = new MaterialDto();
        $docMaterial = new DOMDocument();
        libxml_use_internal_errors(true);
        $docMaterial->preserveWhiteSpace = false;
        $docMaterial->loadHTML($this->getInnerHtml($material));

        $xpathMaterial = new DOMXPath($docMaterial);
        $dto->prices = $this->getPrices($xpathMaterial);
        foreach ($dto->prices as &$price) {
            $price->unit = utf8_decode($price->unit);
        }
        unset($price);
        /** @var DOMNodeList $relCode */
        $relCode = $xpathMaterial->query(ParserHelper::NAME_SELECTOR);
        /** @var DOMElement $title */
        $title = $relCode->item(0);
        if ($title === null) {
            return null;
        }
        $dto->name = trim(utf8_decode($title->nodeValue));
        $dto->external_key = $title->getAttribute('data-product-code');
        /** @var DOMElement[] $relPreview */
        $relPreview = $xpathMaterial->query(ParserHelper::PREVIEW_SELECTOR);
        $dto->images = ['https:' . $relPreview[0]->getAttribute('src')];
        $dto->parser = $this->parser;
        $dto->link = 'https://petrovich.ru' . $title->getAttribute('href');

        return $dto;
    }
}
