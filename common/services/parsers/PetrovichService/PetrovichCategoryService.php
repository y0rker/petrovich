<?php
/**
 * User: y0rker
 * Date: 30.05.19
 * Time: 17:47
 */

namespace common\services\parsers\PetrovichService;


use api\interfaces\Service;
use api\services\BaseService;
use common\components\exceptions\ValidationException;
use common\dto\CategoryDto;
use common\helpers\ParserHelper;
use common\models\Category;
use common\models\Parser;
use common\services\parsers\traits\ParserTrait;
use DOMElement;
use DOMXPath;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class PetrovichCategoryService
 * @package common\services\parser\PetrovichService
 */
class PetrovichCategoryService extends BaseService implements Service
{
    use ParserTrait;

    /** @var Parser */
    private $parser;

    /**
     * @return mixed|void
     * @throws Exception
     * @throws ValidationException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $this->parser = Parser::findOrDie([
            'code'  => PetrovichService::CODE_SERVICE
        ]);
        $doc = $this->getDoc(ParserHelper::CATALOG_URL);

        $xpath = new DOMXPath($doc);
        $categories = $xpath->query(ParserHelper::CATEGORIES_SELECTOR);
        foreach ($categories as $category) {
            $this->parseCategory($category);
        }
    }

    /**
     * @param DOMElement $category
     * @throws ValidationException
     * @throws Exception
     */
    private function parseCategory(DOMElement $category)
    {
        $dto = new CategoryDto();
        $id      = $category->getAttribute('href');
        preg_match('!\d+!', $id, $matches);
        if (!empty($matches[0])) {
            $dto->external_key = (int)$matches[0];
        }
        $dto->name = trim($category->nodeValue);

        $categoryModel = Category::findOrInit([
            'external_key' => $dto->external_key
        ]);
        $categoryModel->name = $dto->name;
        $categoryModel->link = 'https://petrovich.ru' . $id;
        $categoryModel->parser_id = $this->parser->id;
        $categoryModel->saveOrDie();
    }
}
