<?php
/**
 * User: y0rker
 * Date: 20.05.19
 * Time: 13:39
 */

namespace common\services\parsers\PetrovichService;

use common\dto\MaterialDto;
use common\dto\MaterialPropDto;
use common\models\Material;
use common\services\parsers\ParserService;
use common\services\parsers\PetrovichService\traits\PetrovichTrait;
use common\services\parsers\traits\ParserTrait;
use DOMElement;
use DOMXPath;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class PetrovichService
 * @package common\services\parser\PetrovichService
 */
class PetrovichService extends ParserService
{
    use PetrovichTrait;
    use ParserTrait;

    /** @var string */
    public const CODE_SERVICE = 'petrovich';

    /**
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function run(): void
    {
        foreach ($this->links as $link) {
            $this->parseMaterialPage($link);
        }
    }

    /**
     * @param string $link
     * @throws Exception
     * @throws NotFoundHttpException
     */
    protected function parseMaterialPage(string $link): void
    {
        $doc = $this->getDoc($link);
        $xpath = new DOMXPath($doc);

        $materialDto = new MaterialDto();
        $materialDto->link = $link;
        $materialDto->name = $this->getName($xpath);
        $materialDto->external_key = $this->getKey($xpath);
        $materialDto->parser = $this->getParser();
        $materialDto->images = $this->getImages($xpath);
        $materialDto->prices = $this->getPrices($xpath);
        $materialDto->description = $this->getDescription($xpath);
        $materialDto->props = $this->getProps($xpath);

        if ($materialDto->isCorrect()) {
            $this->materialsDto[] = $materialDto;
        }
    }

    /**
     * @param DOMXPath $xpath
     * @return string
     */
    protected function getName(DOMXPath $xpath): ?string
    {
        $h1s = $xpath->document->getElementsByTagName('h1');
        if ($h1s === 0) {
            return null;
        }

        return trim($h1s->item(0)->nodeValue);
    }

    /**
     * @param DOMXPath $xpath
     * @return string|null
     */
    protected function getKey(DOMXPath $xpath): ?string
    {
        $keys = $xpath->query('//*[@data-test="product-code"]');
        if ($keys->length === 0) {
            return null;
        }
        /** @var DOMElement[] $keys */
        foreach ($keys as $item) {
            if (empty($item->tagName)) {
                continue;
            }
            if ($item->tagName !== 'span') {
                continue;
            }

            return trim($item->nodeValue);
        }

        return null;
    }

    /**
     * @param DOMXPath $xpath
     * @return array
     */
    private function getImages(DOMXPath $xpath): array
    {
        $images = $xpath->query('//*[@class="image-gallery-image"]/img');
        $result = [];
        /** @var DOMElement[] $images */
        foreach ($images as $item) {
            if (empty($item->tagName)) {
                continue;
            }

            $src = $item->getAttribute('srcset');
            if (empty($src)) {
                $src = $item->getAttribute('src');
            }
            if (empty($src)) {
                continue;
            }
            $result[] = $src;
        }

        return $result;
    }

    /**
     * @param DOMXPath $xpath
     * @return null|string
     */
    private function getDescription(DOMXPath $xpath): ?string
    {
        $price = $xpath->query('//*[@data-test="product-description"]');
        if ($price->length !== 1) {
            return null;
        }

        /** @var DOMElement $description */
        $description = $price->item(0);

        return trim($description->nodeValue);
    }

    /**
     * @param DOMXPath $xpath
     * @return MaterialPropDto[]
     */
    private function getProps(DOMXPath $xpath): array
    {
        $props = [];
        $params = $xpath->query('//*[@class="specification--table"]/table/tbody/tr');
        /** @var DOMElement[] $params */
        foreach ($params as $param) {
            $tds = $param->getElementsByTagName('td');
            if ($tds->length !== 2) {
                continue;
            }
            $materialParamProp = new MaterialPropDto();
            $materialParamProp->name = trim($tds->item(0)->nodeValue);
            $materialParamProp->value = trim($tds->item(1)->nodeValue);
            $props[] = $materialParamProp;
        }

        return $props;
    }

    /**
     * @throws InvalidConfigException
     */
    protected function parseSitemap(): void
    {
        $this->links = Material::find()
            ->select('link')
            ->asArray()
            ->column();
    }

    /**
     * @return string
     */
    protected function getParserCode(): string
    {
        return self::CODE_SERVICE;
    }

    /**
     * @throws Exception
     */
    protected function save(): void
    {
        SaveMaterials::getInstance()
            ->setMaterialsDto($this->materialsDto)
            ->run();
    }
}
