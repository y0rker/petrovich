<?php
/**
 * User: y0rker
 * Date: 30.05.19
 * Time: 17:41
 */

namespace common\services\parsers\PetrovichService\traits;

use common\dto\MaterialPriceDto;
use DOMElement;
use DOMXPath;

/**
 * Trait PetrovichTrait
 * @package common\services\parser\PetrovichService\traits
 */
trait PetrovichTrait
{
    /**
     * @param DOMXPath $xpath
     * @return MaterialPriceDto[]
     */
    private function getPrices(DOMXPath $xpath): array
    {
        $price = $xpath->query('//*[@data-test="product-retail-price"]');
        if ($price->length !== 1) {
            return [];
        }
        /** @var DOMElement $priceItem */
        $priceItem = $price->item(0);

        $defaultPrice = new MaterialPriceDto();
        $defaultPrice->price = $priceItem->getAttribute('data-default-price');
        $defaultPrice->price = str_replace(',', '.', $defaultPrice->price);
        $defaultPrice->price = str_replace(' ', '', $defaultPrice->price);

        $unitPrice = new MaterialPriceDto();
        $unitPrice->price = $priceItem->getAttribute('data-unit-price');
        $unitPrice->price = str_replace(',', '.', $unitPrice->price);
        $unitPrice->price = str_replace(' ', '', $unitPrice->price);

        $priceUnits = $xpath->query('//*[@class="unit--wrapper"]/div');
        /** @var DOMElement[] $priceUnits */
        foreach ($priceUnits as $priceUnit) {
            if (empty($priceUnit->tagName)) {
                continue;
            }
            if (!$this->hasClass($priceUnit, 'unit--select')) {
                continue;
            }
            $unitType = $priceUnit->getAttribute('data-unit-type');
            if (empty($unitType)) {
                continue;
            }
            if ($unitType === 'alternate') {
                $unitPrice->unit = trim($priceUnit->nodeValue);
            }
            if ($unitType === 'default') {
                $defaultPrice->unit = trim($priceUnit->nodeValue);
            }
        }

        $prices = [];
        if ($defaultPrice->isCorrect()) {
            $prices[] = $defaultPrice;
        }
        if ($unitPrice->isCorrect()) {
            $prices[] = $unitPrice;
        }

        return $prices;
    }

    /**
     * @param DOMElement $item
     * @param string     $className
     * @return bool
     */
    protected function hasClass(DOMElement $item, string $className): bool
    {
        $class = $item->getAttribute('class');
        $classArray = explode(' ', $class);

        return in_array($className, $classArray, true);
    }
}