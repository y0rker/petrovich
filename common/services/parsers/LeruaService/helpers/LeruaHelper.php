<?php
/**
 * User: y0rker
 * Date: 31.05.19
 * Time: 14:57
 */

namespace common\services\parsers\LeruaService\helpers;

/**
 * Class LeruaHelper
 * @package common\services\parser\LeruaService\helpers
 */
class LeruaHelper
{
    public const CATALOG_URL = 'https://spb.leroymerlin.ru/catalogue/';
    public const SITEMAP_URL = 'https://spb.leroymerlin.ru/sitemap.xml';
}
