<?php
/**
 * User: y0rker
 * Date: 31.05.19
 * Time: 15:48
 */

namespace common\services\parsers\LeruaService;


use api\interfaces\Service;
use api\services\BaseService;
use common\dto\MaterialDto;
use common\dto\MaterialPriceDto;
use common\models\Category;
use common\models\Parser;
use common\services\parsers\SaveMaterials;
use common\services\parsers\traits\ParserTrait;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use Throwable;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

/**
 * Class LeruaCatalogService
 * @package common\services\parser\LeruaService
 */
class LeruaCatalogService extends BaseService implements Service
{
    use ParserTrait;

    /** @var MaterialDto[] */
    private $materials = [];
    /** @var Parser */
    private $parser;

    /**
     * PetrovichCatalogService constructor.
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function __construct()
    {
        $this->parser = Parser::findOrDie([
            'code' => LeruaService::CODE_SERVICE
        ]);
    }

    /**
     * @return mixed|void
     * @throws InvalidConfigException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function run()
    {
        $categories = Category::find()
            ->select(['id', 'name', 'external_key', 'link'])
            ->where([
                'parser_id' => $this->parser->id
            ])
            ->all();
        $i = 1;
        foreach ($categories as $category) {
            echo 'Категория №' . $i . PHP_EOL;
            $this->materials = [];
            $this->parseUrl($category->link, $category);

            SaveMaterials::getInstance()
                ->setMaterialsDto($this->materials)
                ->run();

            $i++;
        }
    }

    /**
     * @param string   $link
     * @param Category $category
     * @param bool     $parsePages
     * @throws StaleObjectException
     * @throws Throwable
     */
    private function parseUrl(string $link, Category $category, bool $parsePages = true)
    {
        $doc = $this->getDoc($link);
        $xpath = new DOMXPath($doc);

        /** @var DOMNodeList $materials */
        $materials = $xpath->query('//*[@class="ui-product-card"]');
        if ($materials->length === 0) {
            $category->delete();
        }
        foreach ($materials as $material) {
            $dto = $this->getDto($material);
            if ($dto === null) {
                continue;
            }
            $this->materials[$dto->external_key] = $dto;
        }
        if (!$parsePages) {
            return;
        }
        $totalPages = $this->getTotalPages($xpath);
        for ($i = 2; $i <= $totalPages; $i++) {
            $this->parseUrl($link . '?page=' . $i, $category, false);
        }
    }

    /**
     * @param DOMElement $material
     * @return MaterialDto|null
     */
    private function getDto(DOMElement $material): ?MaterialDto
    {
        $dto = new MaterialDto();
        $docMaterial = new DOMDocument();
        libxml_use_internal_errors(true);
        $docMaterial->preserveWhiteSpace = false;
        $docMaterial->loadHTML($this->getInnerHtml($material));
        $xpathMaterial = new DOMXPath($docMaterial);

        /** @var DOMNodeList $relCode */
        $relCode = $xpathMaterial->query('//*[@class="product-name"]/a');
        /** @var DOMElement $title */
        $title = $relCode->item(0);
        if ($title === null) {
            return null;
        }

        $dto->prices = $this->getPrices($xpathMaterial);
        $dto->name = trim(utf8_decode($title->nodeValue));
        $dto->parser = $this->parser;
        $dto->link = 'https://petrovich.ru' . $title->getAttribute('href');

        /** @var DOMElement[] $relPreview */
        $relPreview = $xpathMaterial->query('//*[@class="ui-product-card__img"]/picture/img');
        $dto->images = [$relPreview[0]->getAttribute('src')];

        /** @var DOMElement[] $info */
        $info = $xpathMaterial->query('//*[@class="ui-product-card-info__item madein"]/span');
        $articleFound = false;
        foreach ($info as $item) {
            if (empty($item->tagName)) {
                continue;
            }
            $nodeValue = utf8_decode($item->nodeValue);
            if ($nodeValue === 'Артикул:') {
                $articleFound = true;
                continue;
            }
            if ($articleFound) {
                $dto->external_key = trim($nodeValue);
                break;
            }
        }

        return $dto;
    }

    /**
     * @param DOMXPath $xpathMaterial
     * @return MaterialPriceDto[]
     */
    private function getPrices(DOMXPath $xpathMaterial): array
    {
        $price = $xpathMaterial->query('//*[@itemprop="price"]');
        /** @var DOMElement $priceItem */
        $priceItem = $price->item(0);

        $defaultPrice = new MaterialPriceDto();
        $defaultPrice->price = $priceItem->getAttribute('content');
        $defaultPrice->price = str_replace(',', '.', $defaultPrice->price);
        $defaultPrice->price = str_replace(' ', '', $defaultPrice->price);

        /** @var DOMElement[] $priceUnit */
        $priceUnit = $xpathMaterial->query('//*[@class="unit"]/span');
        foreach ($priceUnit as $unit) {
            if (empty($unit->tagName)) {
                continue;
            }
            if ($unit->getAttribute('class') === 'item') {
                $defaultPrice->unit = trim(utf8_decode($unit->nodeValue));
                break;
            }
        }

        return [$defaultPrice];
    }

    /**
     * @param DOMXPath $xpath
     * @return int
     */
    private function getTotalPages(DOMXPath $xpath): int
    {
        /** @var DOMNodeList $pagination */
        $pagination = $xpath->query('//*[@class="items-wrapper"]/div');
        $lastPag = $pagination->item($pagination->length - 1);
        if ($lastPag === null) {
            return 0;
        }

        return (int)trim($lastPag->nodeValue);
    }
}
