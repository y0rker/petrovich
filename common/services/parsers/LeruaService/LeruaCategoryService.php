<?php
/**
 * User: y0rker
 * Date: 31.05.19
 * Time: 14:55
 */

namespace common\services\parsers\LeruaService;


use api\interfaces\Service;
use api\services\BaseService;
use common\components\exceptions\ValidationException;
use common\models\Category;
use common\models\Parser;
use common\services\parsers\LeruaService\helpers\LeruaHelper;
use common\services\parsers\PetrovichService\PetrovichService;
use common\services\parsers\traits\ParserTrait;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class LeruaCategoryService
 * @package common\services\parser\LeruaService
 */
class LeruaCategoryService extends BaseService implements Service
{
    use ParserTrait;

    /** @var Parser */
    private $parser;
    /** @var array */
    private $categories = [];

    /**
     * @return mixed|void
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws ValidationException
     */
    public function run()
    {
        $this->parser = Parser::findOrDie([
            'code' => LeruaService::CODE_SERVICE
        ]);
        $data = $this->getSitemap(LeruaHelper::SITEMAP_URL);
        $locations = [];
        foreach ($data['sitemap'] ?? [] as $sitemap) {
            $locations[] = $sitemap['loc'];
        }
        foreach ($locations as $location) {
            $data = $this->getSitemap($location);
            $this->bindCategories($data);
        }
        $this->saveCategories();
    }

    /**
     * @param string $url
     * @return array
     */
    private function getSitemap(string $url): array
    {
        $xmlfile = file_get_contents($url);
        $ob = simplexml_load_string($xmlfile);
        $json = json_encode($ob);
        return json_decode($json, true);
    }

    /**
     * @throws Exception
     * @throws ValidationException
     */
    private function saveCategories()
    {
        foreach ($this->categories as $category) {
            $key = str_replace(LeruaHelper::CATALOG_URL,'', $category);
            $categoryModel = Category::findOrInit([
                'external_key' => $key
            ]);
            $categoryModel->name = $key;
            $categoryModel->link = $category;
            $categoryModel->parser_id = $this->parser->id;
            $categoryModel->saveOrDie();
        }
    }

    /**
     * @param array $data
     */
    private function bindCategories(array $data)
    {
        foreach ($data['url'] ?? [] as $datum) {
            $loc = $datum['loc'];
            if ($loc !== LeruaHelper::CATALOG_URL && strpos($loc, LeruaHelper::CATALOG_URL) !== false) {
                $this->categories[] = $loc;
            }
        }
    }
}