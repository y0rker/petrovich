<?php
/**
 * User: y0rker
 * Date: 31.05.19
 * Time: 14:52
 */

namespace common\services\parsers\LeruaService;

use common\services\parsers\ParserService;

/**
 * Class LeruaService
 * @package common\services\parser\LeruaService
 */
class LeruaService extends ParserService
{
    /** @var string */
    public const CODE_SERVICE = 'lerya';

    /**
     * @return string
     */
    protected function getParserCode(): string
    {
        return self::CODE_SERVICE;
    }

    /**
     * @return void
     */
    protected function parseSitemap(): void
    {
        // TODO: Implement parseSitemap() method.
    }

    /**
     * @return void
     */
    protected function save(): void
    {
        // TODO: Implement save() method.
    }

    /**
     * @param string $link
     * @return void
     */
    protected function parseMaterialPage(string $link): void
    {
        // TODO: Implement parseMaterialPage() method.
    }

    /**
     * @return mixed
     */
    public function run()
    {
        // TODO: Implement run() method.
    }
}
