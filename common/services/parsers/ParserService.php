<?php
/**
 * User: y0rker
 * Date: 20.05.19
 * Time: 14:16
 */

namespace common\services\parsers;

use api\interfaces\Service;
use api\services\BaseService;
use common\dto\MaterialDto;
use common\models\Parser;
use DOMElement;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class ParserService
 * @package common\services\parser
 */
abstract class ParserService extends BaseService implements Service
{
    /** @var array */
    protected $links = [];
    /** @var MaterialDto[] */
    protected $materialsDto = [];
    /** @var Parser */
    protected $parser;

    /**
     * ParserService constructor.
     */
    public function __construct()
    {
        $this->parseSitemap();
    }

    /**
     * @return void
     */
    public function __destruct()
    {
        $this->save();
    }

    /**
     * @return Parser
     * @throws Exception
     * @throws NotFoundHttpException
     */
    protected function getParser(): Parser
    {
        if ($this->parser !== null) {
            return $this->parser;
        }

        $parserCode = $this->getParserCode();
        $this->parser = Parser::findOrDie([
            'code'    => $parserCode,
            'visible' => true
        ]);

        return $this->parser;
    }

    /**
     * @return string
     */
    abstract protected function getParserCode(): string;

    /**
     * @param DOMElement $item
     * @param string     $className
     * @return bool
     */
    protected function hasClass(DOMElement $item, string $className): bool
    {
        $class = $item->getAttribute('class');
        $classArray = explode(' ', $class);

        return in_array($className, $classArray, true);
    }

    /**
     * @return void
     */
    abstract protected function parseSitemap(): void;

    /**
     * @return void
     */
    abstract protected function save(): void;

    /**
     * @param string $link
     * @return void
     */
    abstract protected function parseMaterialPage(string $link): void;
}
