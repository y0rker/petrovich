<?php
/**
 * User: y0rker
 * Date: 30.05.19
 * Time: 18:19
 */

namespace common\services\parsers;

use api\interfaces\Service;
use api\services\BaseService;
use common\dto\MaterialDto;
use common\helpers\DbHelper;
use common\models\Material;
use common\models\MaterialPrice;
use common\models\MaterialProp;
use PDO;
use Throwable;
use Yii;
use yii\db\Expression;

/**
 * Class PetrovichSaveMaterials
 * @package common\services\parser\PetrovichService
 */
class SaveMaterials extends BaseService implements Service
{
    /** @var MaterialDto[] */
    protected $materialsDto = [];

    /**
     * @return void
     */
    public function run(): void
    {
        $inserts = [];
        $date = date('Y-m-d H:i:s');
        foreach ($this->materialsDto as $materialDto) {
            $inserts[] = [
                $materialDto->external_key,
                $materialDto->name,
                $date,
                $materialDto->link,
                $materialDto->parser->id,
                $materialDto->images,
                $materialDto->description,
                $materialDto->vendor,
                $materialDto->article,
                $materialDto->category_id ?? null
            ];
        }
        if (empty($inserts)) {
            return;
        }
        $command = Yii::$app->getDb()->createCommand()
            ->batchInsert(Material::tableName(), [
                'external_key',
                'name',
                'created',
                'link',
                'parser_id',
                'images',
                'description',
                'vendor',
                'article',
                'category_id'
            ], $inserts);

        $sql = DbHelper::addOnConflictUpdate(
            $command,
            ['external_key', 'parser_id'],
            [
                'name',
                'link',
                'images',
                'description',
                'category_id',
                ['updated' => 'NOW()'],
            ]
        );

        $sql = DbHelper::addReturningBySql($sql, [
            'id',
            'external_key',
            'parser_id',
            new Expression('(SELECT datestamp FROM material_price WHERE material_id = material.id ORDER BY datestamp DESC LIMIT 1)')
        ]);

        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $insertPrices = [];
            $insertProps = [];
            /** @var MaterialDto[] $materialsDto */
            $materialsDto = Yii::$app->getDb()->createCommand($sql)->queryAll([PDO::FETCH_CLASS, MaterialDto::class]);
            foreach ($materialsDto as $materialDto) {
                $parsedMaterialDto = $this->getParsedMaterialDto($materialDto);
                if ($parsedMaterialDto === null) {
                    continue;
                }
                foreach ($parsedMaterialDto->prices ?? [] as $materialPriceDto) {
                    $insertPrices[] = [
                        $materialDto->id,
                        $materialPriceDto->price,
                        $materialDto->datestamp ?? $date,
                        $date,
                        $materialPriceDto->unit
                    ];
                }
                foreach ($parsedMaterialDto->props ?? [] as $materialPropDto) {
                    $insertProps[] = [
                        $materialDto->id,
                        $materialPropDto->name,
                        $materialPropDto->value,
                        $date
                    ];
                }
            }

            if (!empty($insertPrices)) {
                $commandPrices = Yii::$app->getDb()->createCommand()
                    ->batchInsert(MaterialPrice::tableName(), [
                        'material_id',
                        'price',
                        'datestamp',
                        'created',
                        'unit',
                    ], $insertPrices);

                $sql = DbHelper::addOnConflictUpdate(
                    $commandPrices,
                    ['material_id', 'price', 'datestamp']
                );
                Yii::$app->getDb()->createCommand($sql)->execute();
            }
            if (!empty($insertProps)) {
                $commandProps = Yii::$app->getDb()->createCommand()
                    ->batchInsert(MaterialProp::tableName(), [
                        'material_id',
                        'name',
                        'value',
                        'created'
                    ], $insertProps);

                $sql = DbHelper::addOnConflictUpdate(
                    $commandProps,
                    ['material_id', 'name'],
                    [
                        'value',
                        ['old_value' => MaterialProp::tableColumn('value')],
                        ['updated' => 'NOW()'],
                    ]
                );
                Yii::$app->getDb()->createCommand($sql)->execute();
            }

            $transaction->commit();
        } catch (Throwable $exception) {
            $transaction->rollBack();
        }
    }

    /**
     * @param MaterialDto $materialDto
     * @return MaterialDto|null
     */
    private function getParsedMaterialDto(MaterialDto $materialDto): ?MaterialDto
    {
        foreach ($this->materialsDto as $item) {
            if ($item->external_key === (string)$materialDto->external_key &&
                $item->parser->id === $materialDto->parser_id) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param MaterialDto[] $materialsDto
     * @return SaveMaterials
     */
    public function setMaterialsDto(array $materialsDto): SaveMaterials
    {
        $this->materialsDto = $materialsDto;

        return $this;
    }
}
