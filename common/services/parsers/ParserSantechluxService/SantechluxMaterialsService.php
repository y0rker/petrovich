<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 17:32
 */

namespace common\services\parsers\ParserSantechluxService;

use common\dto\MaterialDto;
use common\dto\MaterialPriceDto;
use common\dto\ParserMaterialCategoryDto;
use common\services\ParserMaterialsServiceBase;
use common\services\parsers\ParserSantechluxService\helpers\SantechluxHelper;
use common\services\parsers\SaveMaterials;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use yii\web\ServerErrorHttpException;

/**
 * Class SantechluxMaterialsService
 * @package common\services\parsers\ParserSantechluxService
 */
class SantechluxMaterialsService extends ParserMaterialsServiceBase
{
    /** @var MaterialDto[] */
    private $materials;

    /**
     * @throws ServerErrorHttpException
     */
    protected function parse(): void
    {
        foreach ($this->materialsUrl as $material) {
            $dto = $this->parseMaterial($material);
            if ($dto === null) {
                continue;
            }

            $this->materials[] = $dto;
        }

        SaveMaterials::getInstance()
            ->setMaterialsDto($this->materials)
            ->run();
    }

    /**
     * @param ParserMaterialCategoryDto $materialCategoryDto
     * @return MaterialDto|null
     * @throws ServerErrorHttpException
     */
    private function parseMaterial(ParserMaterialCategoryDto $materialCategoryDto): ?MaterialDto
    {
        $dto = new MaterialDto();
        $dto->category_id = $materialCategoryDto->category_id;

        $docMaterial = $this->getDoc($materialCategoryDto->materialUrl);
        if ($docMaterial === null) {
            return null;
        }

        $xpathMaterial = new DOMXPath($docMaterial);
        $dto->prices = $this->getPrices($xpathMaterial);
        foreach ($dto->prices as &$price) {
            if (empty($price->unit)) {
                continue;
            }
            $price->unit = utf8_decode($price->unit);
        }
        unset($price);
        /** @var DOMNodeList $relCode */
        $relCode = $xpathMaterial->query(SantechluxHelper::MATERIAL_TITLE);
        /** @var DOMElement $title */
        $title = $relCode->item(0);
        if ($title === null) {
            return null;
        }
        $dto->name = trim($title->nodeValue);

        /** @var DOMElement[] $relPreview */
        $relPreview = $xpathMaterial->query(SantechluxHelper::MATERIAL_IMAGE);
        $dto->images = [$relPreview[0]->getAttribute('href')];
        $dto->parser = $this->parser;
        $dto->link = $materialCategoryDto->materialUrl;

        /** @var DOMElement[] $props */
        $props = $xpathMaterial->query(SantechluxHelper::MATERIAL_PROPS);
        foreach ($props as $prop) {
            /** @var DOMElement $var */
            $var = $prop->firstChild;
            /** @var DOMElement $value */
            $value = $prop->lastChild;
            $nameVar = trim($var->nodeValue);
            $nameValue = trim($value->nodeValue);
            if ($nameVar === 'Код товара: ') {
                $dto->external_key = $nameValue;
                continue;
            }
            if ($nameVar === 'Артикул: ') {
                $dto->article = $nameValue;
                continue;
            }
            if ($nameVar === 'Производитель: ') {
                $dto->vendor = $nameValue;
            }
        }

        return $dto;
    }

    /**
     * @param DOMXPath $xpath
     * @return MaterialPriceDto[]
     */
    private function getPrices(DOMXPath $xpath): array
    {
        $price = $xpath->query('//*[@class="product_price p-p-b"]');
        if ($price->length !== 1) {
            $price = $xpath->query('//*[@class="product_price_stock p-p-b"]');
            if ($price->length !== 1) {
                return [];
            }
        }
        /** @var DOMElement $priceItem */
        $priceItem = $price->item(0);

        $defaultPrice = new MaterialPriceDto();
        $defaultPrice->price = trim($priceItem->nodeValue);
        $defaultPrice->price = str_replace(',', '.', $defaultPrice->price);
        $defaultPrice->price = str_replace(' ', '', $defaultPrice->price);
        $defaultPrice->price = (float)$defaultPrice->price;

        $prices = [];
        if ($defaultPrice->isCorrect()) {
            $prices[] = $defaultPrice;
        }

        return $prices;
    }
}
