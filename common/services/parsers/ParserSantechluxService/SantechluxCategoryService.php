<?php
/**
 * User: y0rker
 * Date: 04.07.19
 * Time: 12:34
 */

namespace common\services\parsers\ParserSantechluxService;

use common\components\dom\DOMXPath;
use common\dto\ParserMaterialCategoryDto;
use common\jobs\ParseMaterialsJob;
use common\services\ParserCategoryServiceBase;
use common\services\parsers\ParserSantechluxService\helpers\SantechluxHelper;
use DOMElement;
use Yii;
use yii\web\ServerErrorHttpException;

/**
 * Class SantechluxCategoryService
 * @package common\services\parser\SantechluxService
 */
class SantechluxCategoryService extends ParserCategoryServiceBase
{
    /**
     * @throws ServerErrorHttpException
     */
    protected function parse(): void
    {
        $this->parseCategory($this->categoryUrl);
    }

    /**
     * @param string $url
     * @param bool   $recursive
     * @return bool
     * @throws ServerErrorHttpException
     */
    private function parseCategory(string $url, bool $recursive = true): bool
    {
        $materialsForJobs = [];
        $doc = $this->getDoc($url);
        $xpath = new DOMXPath($doc);

        $materials = $xpath->query(SantechluxHelper::MATERIAL_SELECTOR);
        foreach ($materials as $material) {
            /** @var DOMElement $href */
            $href = $material->getElementsByTagName('a')->item(0);
            if ($href === null) {
                continue;
            }
            $link = $href->getAttribute('href');
            if ($link[0] !== '/') {
                $link = '/' . $link;
            }
            $link = $this->client->getBaseUrl() . $link;
            $parserMaterialCategoryDto = new ParserMaterialCategoryDto();
            $parserMaterialCategoryDto->category_id = $this->category->id;
            $parserMaterialCategoryDto->materialUrl = $link;

            $materialsForJobs[] = $parserMaterialCategoryDto;
        }

        Yii::$app->catalog->push(new ParseMaterialsJob([
            'materialsUrl' => $materialsForJobs,
            'clientId'     => $this->client->getId()
        ]));

        if ($recursive) {
            $i = 1;
            do {
                $break = $this->parseCategory($url . '&limit=16&limitstart=' . $i * 16, false);
                if ($break) {
                    break;
                }
                $i++;
            } while (true);
        }

        return false;
    }
}
