<?php
/**
 * User: y0rker
 * Date: 04.07.19
 * Time: 12:33
 */

namespace common\services\parsers\ParserSantechluxService\helpers;

/**
 * Class SantechluxHelper
 * @package common\services\parser\SantechluxService\helpers
 */
class SantechluxHelper
{
    public const MATERIAL_SELECTOR = '//*[contains(@class,\'browseProductContainer\')]';
    public const MATERIAL_TITLE    = '//*/h1';
    public const MATERIAL_IMAGE    = '//*[@class="product_image"]/*/a';
    public const MATERIAL_PROPS    = '//*[@id="vmMainPage"]/table[1]/tr/td[2]/table/tr[4]/td/table/tr';

    public const MATERIAL_PRICE        = '//*[@class="product_price p-p-b"]';
    public const MATERIAL_PRICE_SECOND = '//*[@class="product_price_stock p-p-b"]';
}
