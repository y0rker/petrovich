<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 17:32
 */

namespace common\services;

use api\interfaces\ParserInterface;
use api\interfaces\Service;
use api\services\BaseService;
use common\dto\ParserMaterialCategoryDto;
use common\models\Parser;
use common\traits\ParserTrait;

/**
 * Class ParserMaterialsServiceBase
 * @package common\services
 */
abstract class ParserMaterialsServiceBase extends BaseService implements Service
{
    use ParserTrait;

    /** @var ParserInterface */
    protected $client;
    /** @var ParserMaterialCategoryDto[] */
    protected $materialsUrl = [];
    /** @var Parser */
    protected $parser;

    /**
     * @return mixed
     */
    public function run(): void
    {
        $this->parser = Parser::findOrCreate([
            'name' => $this->client->getName(),
            'code' => $this->client->getId(),
            'link' => $this->client->getBaseUrl()
        ]);

        $this->parse();
    }

    /**
     * @return void
     */
    abstract protected function parse(): void;

    /**
     * @param ParserInterface $client
     * @return ParserMaterialsServiceBase
     */
    public function setClient(ParserInterface $client): ParserMaterialsServiceBase
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @param ParserMaterialCategoryDto[] $materialsUrl
     * @return ParserMaterialsServiceBase
     */
    public function setMaterialsUrl(array $materialsUrl): ParserMaterialsServiceBase
    {
        $this->materialsUrl = $materialsUrl;

        return $this;
    }
}
