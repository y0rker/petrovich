<?php

use \common\components\Migration;

/**
 * Class m190529_155739_addMaterialPriceUnit
 */
class m190529_155739_addMaterialPriceUnit extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material_price' => [
                'unit' => $this->string()->null()
            ]
        ];
    }
}
