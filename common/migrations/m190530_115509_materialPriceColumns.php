<?php

use \common\components\Migration;

/**
 * Class m190530_115509_materialPriceColumns
 */
class m190530_115509_materialPriceColumns extends Migration
{
    /**
     * @return array
     */
    public function setIndexes(): array
    {
        return [
            'material_price_created' => ['material_price', ['material_id', 'price', 'datestamp'], true],
        ];
    }

    /**
     * @return array
     */
    protected function setFields()
    {
        return [
            'material_price' => [
                'created' => $this->dateTime()
            ]
        ];
    }
}
