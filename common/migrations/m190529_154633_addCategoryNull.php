<?php

use \common\components\Migration;

/**
 * Class m190529_154633_addCategoryNull
 */
class m190529_154633_addCategoryNull extends Migration
{
    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        Yii::$app->getDb()->createCommand('alter table material alter column category_id drop not null;')->execute();
        return parent::safeUp();
    }
}
