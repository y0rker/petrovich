<?php

use \common\components\Migration;

/**
 * Class m190704_125300_addArticle
 */
class m190704_125300_addArticle extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material'  => [
                'article'    => $this->string()->null()
            ]
        ];
    }
}
