<?php

use \common\components\Migration;

/**
 * Class m190530_133659_materialPropColumns
 */
class m190530_133659_materialPropColumns extends Migration
{
    /**
     * @return array
     */
    public function setForeignKeys(): array
    {
        return [
            [
                'material_prop' => 'material_id',
                'material'      => 'id'
            ],
            [
                'material_price' => 'material_id',
                'material'    => 'id'
            ]
        ];
    }

    /**
     * @return array
     */
    public function setIndexes(): array
    {
        return [
            'material_prop_index' => ['material_prop', ['material_id', 'name'], true],
        ];
    }
}
