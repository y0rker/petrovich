<?php

use \common\components\Migration;

/**
 * Class m190529_155027_addMaterialImages
 */
class m190529_155027_addMaterialImages extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material'  => [
                'images'    => $this->jsonb()->defaultValue('[]')
            ]
        ];
    }
}
