<?php

use \common\components\Migration;

/**
 * Class m190529_163256_addDescriptionMaterial
 */
class m190529_163256_addDescriptionMaterial extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material' => [
                'description' => $this->text()->null(),
            ]
        ];
    }

    /**
     * @return array
     */
    public function setTables(): array
    {
        return [
            'material_prop' => [
                'id'          => $this->primaryKey(),
                'material_id' => $this->integer()->notNull(),
                'name'        => $this->string()->notNull(),
                'value'       => $this->string()->notNull(),
                'old_value'   => $this->string(),
                'created'     => $this->dateTime(),
                'updated'     => $this->dateTime()
            ]
        ];
    }
}
