<?php

use \common\components\Migration;

/**
 * Class m190710_134619_dropBadColumns
 */
class m190710_134619_dropBadColumns extends Migration
{
    /**
     * @return bool
     */
    public function safeUp(): bool
    {
        $this->dropColumn('category', 'external_key');
        $this->dropColumn('category', 'name');
        return parent::safeUp();
    }
}
