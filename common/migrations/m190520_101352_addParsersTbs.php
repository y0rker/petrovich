<?php

use \common\components\Migration;

/**
 * Class m190520_101352_addParsersTbs
 */
class m190520_101352_addParsersTbs extends Migration
{
    /** @inheritdoc */
    public function setTables(): array
    {
        return [
            'parser' => [
                'id'      => $this->primaryKey(),
                'name'    => $this->string()->notNull(),
                'code'    => $this->string()->notNull(),
                'link'    => $this->string()->notNull(),
                'visible' => $this->boolean()->defaultValue(true)
            ],
        ];
    }

    /**
     * @return array
     */
    protected function setFields()
    {
        return [
            'category' => [
                'link'      => $this->string()->null(),
                'parser_id' => $this->integer()->null(),
            ],
            'material' => [
                'link'      => $this->string()->null(),
                'parser_id' => $this->integer()->null(),
            ]
        ];
    }
}
