<?php

use \common\components\Migration;

/**
 * Class m190520_103046_setDataParser
 */
class m190520_103046_setDataParser extends Migration
{
    /**
     * @return array
     */
    public function setValues()
    {
        return [
            'parser' => [
                [
                    'name'    => 'Петрович',
                    'code'    => 'petrovich',
                    'link'    => 'https://petrovich.ru/',
                    'visible' => true
                ],
                [
                    'name'    => 'Максидом',
                    'code'    => 'maxidom',
                    'link'    => 'https://www.maxidom.ru/',
                    'visible' => true
                ],
                [
                    'name'    => 'Леруа Мерлен',
                    'code'    => 'lerya',
                    'link'    => 'https://leroymerlin.ru/',
                    'visible' => true
                ],
                [
                    'name'    => 'Обойкин',
                    'code'    => 'oboykin',
                    'link'    => 'http://www.oboykin.ru/',
                    'visible' => true
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    public function setForeignKeys()
    {
        return [
            [
                'category' => 'parser_id',
                'parser'   => 'id'
            ],
            [
                'material' => 'parser_id',
                'parser'   => 'id'
            ]
        ];
    }
}
