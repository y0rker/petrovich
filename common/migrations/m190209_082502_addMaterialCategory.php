<?php

use \common\components\Migration;

/**
 * Class m190209_082502_addMaterialCategory
 */
class m190209_082502_addMaterialCategory extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material' => [
                'category_id' => $this->integer()->notNull()
            ]
        ];
    }

    /**
     * @return array
     */
    public function setForeignKeys(): array
    {
        return [
            [
                'material' => 'category_id',
                'category' => 'id'
            ]
        ];
    }
}
