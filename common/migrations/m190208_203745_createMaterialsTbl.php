<?php

use \common\components\Migration;

/**
 * Class m190208_203745_createMaterialsTbl
 */
class m190208_203745_createMaterialsTbl extends Migration
{
    /** @inheritdoc */
    public function setTables(): array
    {
        return [
            'material'       => [
                'id'            => $this->primaryKey(),
                'petrovich_key' => $this->integer()->notNull(),
                'name'          => $this->string()->notNull(),
                'preview'       => $this->string(500)->null(),
                'created'       => $this->dateTime()
            ],
            'material_price' => [
                'id'          => $this->primaryKey(),
                'material_id' => $this->integer()->notNull(),
                'price'       => $this->price()->notNull(),
                'created'     => $this->dateTime()
            ]
        ];
    }
}
