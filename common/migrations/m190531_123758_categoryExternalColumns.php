<?php

use \common\components\Migration;

/**
 * Class m190531_123758_categoryExternalColumns
 */
class m190531_123758_categoryExternalColumns extends Migration
{
    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        Yii::$app->getDb()->createCommand('alter table category alter column external_key type varchar(255) using external_key::varchar(255);')->execute();
        return parent::safeUp(); // TODO: Change the autogenerated stub
    }
}
