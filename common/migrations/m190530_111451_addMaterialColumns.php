<?php

use \common\components\Migration;

/**
 * Class m190530_111451_addMaterialColumns
 */
class m190530_111451_addMaterialColumns extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material' => [
                'updated' => $this->dateTime()
            ]
        ];
    }

    /**
     * @return array
     */
    public function setIndexes(): array
    {
        return [
            'external_parser' => ['material', ['external_key', 'parser_id'], true],
        ];
    }
}
