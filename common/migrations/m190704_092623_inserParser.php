<?php

use \common\components\Migration;

/**
 * Class m190704_092623_inserParser
 */
class m190704_092623_inserParser extends Migration
{
    /** @inheritdoc */
    public function setValues(): array
    {
        return [
            'parser' => [
                [
                    'name'    => 'Artemix',
                    'code'    => 'santechlux',
                    'link'    => 'http://www.santech-lux.ru',
                    'visible' => true
                ]
            ]
        ];
    }
}
