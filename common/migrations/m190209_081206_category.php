<?php

use \common\components\Migration;

/**
 * Class m190209_081206_category
 */
class m190209_081206_category extends Migration
{
    /** @inheritdoc */
    public function setTables(): array
    {
        return [
            'category' => [
                'id'            => $this->primaryKey(),
                'name'          => $this->string()->notNull(),
                'petrovich_key' => $this->integer()->notNull()->unique(),
                'created'       => $this->dateTime()
            ]
        ];
    }
}
