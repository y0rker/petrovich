<?php

use \common\components\Migration;

/**
 * Class m190530_114259_materialPriceIndex
 */
class m190530_114259_materialPriceIndex extends Migration
{
    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        Yii::$app->getDb()->createCommand('alter table material_price alter column created type date using created::date;')->execute();
        Yii::$app->getDb()->createCommand('alter table material_price rename column created to datestamp;')
            ->execute();
        return parent::safeUp();
    }
}
