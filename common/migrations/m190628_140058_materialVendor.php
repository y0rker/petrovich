<?php

use \common\components\Migration;

/**
 * Class m190628_140058_materialVendor
 */
class m190628_140058_materialVendor extends Migration
{
    /** @inheritdoc */
    public function setFields(): array
    {
        return [
            'material'  => [
                'vendor'    => $this->string()->null()
            ]
        ];
    }
}
