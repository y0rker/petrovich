<?php
/**
 * User: y0rke
 * Date: 08.02.2019
 * Time: 22:22
 */

namespace common\dto;

use common\models\Parser;

/**
 * Class MaterialDto
 *
 * @package common\services\dto
 */
class MaterialDto
{
    /** @var int */
    public $id;
    /** @var int */
    public $external_key;
    /** @var string */
    public $article;
    /** @var string */
    public $link;
    /** @var string */
    public $name;
    /** @var string */
    public $description;
    /** @var string[] */
    public $images = [];
    /** @var MaterialPriceDto[] */
    public $prices = [];
    /** @var MaterialPropDto[] */
    public $props = [];
    /** @var Parser */
    public $parser;
    /** @var int */
    public $parser_id;
    /** @var string */
    public $datestamp;
    /** @var string */
    public $vendor;
    /** @var int */
    public $category_id;

    /**
     * @return bool
     */
    public function isCorrect(): bool
    {
        return !(empty($this->external_key) || empty($this->link) || empty($this->name) || empty($this->prices));
    }
}
