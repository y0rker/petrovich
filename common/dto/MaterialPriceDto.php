<?php
/**
 * User: y0rker
 * Date: 29.05.19
 * Time: 18:59
 */

namespace common\dto;

/**
 * Class MaterialPriceDto
 * @package common\dto
 */
class MaterialPriceDto
{
    /** @var string */
    public $price;
    /** @var string */
    public $unit;

    /**
     * @return bool
     */
    public function isCorrect(): bool
    {
        return !empty($this->price);
    }
}
