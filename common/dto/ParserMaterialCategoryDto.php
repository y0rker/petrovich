<?php
/**
 * User: y0rker
 * Date: 10.07.19
 * Time: 17:58
 */

namespace common\dto;

/**
 * Class ParserMaterialCategoryDto
 * @package common\dto
 */
class ParserMaterialCategoryDto
{
    /** @var string */
    public $materialUrl;
    /** @var int */
    public $category_id;
}
