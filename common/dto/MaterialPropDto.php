<?php
/**
 * User: y0rker
 * Date: 29.05.19
 * Time: 19:38
 */

namespace common\dto;

/**
 * Class MaterialPropDto
 * @package common\dto
 */
class MaterialPropDto
{
    /** @var string */
    public $name;
    /** @var string */
    public $value;
}
