<?php
/**
 * User: y0rke
 * Date: 09.02.2019
 * Time: 11:39
 */

namespace common\dto;

/**
 * Class SearchDto
 *
 * @package common\dto
 */
class SearchDto
{
    public $id;
    public $link;
    public $price;
    public $petrovich_key;
    public $name;
    public $preview;
    public $category_name;
    public $category_petrovich_key;
    public $url;
    public $category_url;
    public $images;
    public $vendor;

    /**
     * SearchDto constructor.
     */
    public function __construct()
    {
        $images = json_decode($this->images, true);
        if (!empty($images) && !empty($images[0])) {
            $this->images = $images[0];
        }
    }
}
