<?php
/**
 * User: y0rke
 * Date: 09.02.2019
 * Time: 11:16
 */

namespace common\dto;

/**
 * Class CategoryDto
 *
 * @package common\dto
 */
class CategoryDto
{
    /** @var string */
    public $name;
    /** @var int */
    public $external_key;
}
