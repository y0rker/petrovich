<?php

namespace common\helpers;

use function array_keys;
use function array_values;
use function implode;
use function is_array;
use yii\db\Command;
use yii\db\Expression;

/**
 * Class DbHelper
 *
 * @package common\helpers
 */
class DbHelper
{
    /**
     * @param Command $command
     * @param array   $keys
     * @param array   $columns
     *
     * @return string
     */
    public static function addOnConflictUpdate(Command $command, array $keys, array $columns = []): string
    {
        $sql = $command->getRawSql();
        if (empty($keys)) {
            return $sql;
        }
        if (empty($columns)) {
            return self::addOnConflictNothingBySql($sql, $keys);
        }

        return self::addOnConflictUpdateBySql($sql, $keys, $columns);
    }

    /**
     * @param string $sql
     * @param array  $keys
     * @param array  $columns
     * @return string
     */
    public static function addOnConflictUpdateBySql(string $sql, array $keys, array $columns = []): string
    {
        $key = implode(', ', $keys);

        return $sql . " ON CONFLICT ({$key}) DO UPDATE SET " . static::columns($columns);
    }

    /**
     * @param string $sql
     * @param array  $keys
     * @return string
     */
    public static function addOnConflictNothingBySql(string $sql, array $keys): string
    {
        $key = implode(', ', $keys);

        return $sql . " ON CONFLICT ({$key}) DO NOTHING";
    }

    /**
     * @param Command $command
     * @param array   $columns
     * @return string
     */
    public static function addReturning(Command $command, array $columns = []): string
    {
        $sql = $command->getRawSql();
        if (empty($columns)) {
            return $sql;
        }

        return self::addReturningBySql($sql, $columns);
    }

    /**
     * @param string $sql
     * @param array  $columns
     * @return string
     */
    public static function addReturningBySql(string $sql, array $columns): string
    {
        $arr = [];
        foreach ($columns as $column) {
            if (is_array($column)) {
                $arr[] = array_keys($column)[0] . ' as ' . array_values($column)[0];
            } else {
                $arr[] = $column;
            }
        }

        if (empty($arr)) {
            return $sql;
        }

        return $sql . ' RETURNING ' . implode(', ', $arr);
    }

    /**
     * @param array $columns
     *
     * @return string
     */
    private static function columns(array $columns): string
    {
        $arr = [];
        foreach ($columns as $column) {
            if ($column instanceof Expression) {
                $arr[] = $column;
                continue;
            }
            if (is_array($column)) {
                $arr[] = array_keys($column)[0] . ' = ' . array_values($column)[0];
            } else {
                $arr[] = $column . ' = EXCLUDED.' . $column;
            }
        }

        return implode(', ', $arr);
    }
}
