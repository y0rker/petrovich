<?php
/**
 * User: y0rke
 * Date: 08.02.2019
 * Time: 22:11
 */

namespace common\helpers;

/**
 * Class ParserHelper
 *
 * @package common
 */
class ParserHelper
{
    public const CATALOG_URL = 'https://petrovich.ru/catalog/';
    public const BASE_URL = 'https://petrovich.ru/';

    public const CATEGORIES_SELECTOR = '//*[contains(@class,\'main__catalog-categoty\')]/div[3]/ul/a';
    public const MATERIAL_SELECTOR = '//*[@id="products_section"]/div/div';
    public const CATEGORY_SELECTOR = '/html/body/div[1]/main/div[1]/div[4]/h1';
    public const PRICE_SELECTOR = '//*[contains(@class,\'main-info\')]/div[2]/div[1]/div[2]/span[1]';
    public const NAME_SELECTOR = '//*[@data-test="product-title"]';
    public const TOTAL_COUNT_SELECTOR = '//*[@id="pagination_container"]/p/span[2]';
    public const PREVIEW_SELECTOR = '//*[contains(@class,\'listing__product-photo\')]/span/img';
    //*[@id="products_section"]/div/div[1]/div[1]/a/img

    public const COUNT_ON_PAGE = 20;
}
