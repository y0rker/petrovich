<?php

namespace yii\web {

    use common\components\ParsersComponent;
    use common\components\db\Connection;
    use common\components\queue\amqp\Queue;
    use common\components\sentry\Component;
    use Yii;

    /**
     * Application is the base class for all web application classes.
     *
     * @property Queue            catalog
     * @property ParsersComponent parsers
     * @property Component        sentry
     * @method Connection getDb()
     *
     */
    abstract class Application extends \yii\base\Application
    {
    }
}
