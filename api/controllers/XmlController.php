<?php
/**
 * User: y0rker
 * Date: 28.06.19
 * Time: 14:43
 */

namespace api\controllers;

use api\components\Controller;
use api\services\SearchService;
use common\models\Parser;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class XmlController
 * @package api\controllers
 */
class XmlController extends Controller
{
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionView(int $id)
    {
        $parser = Parser::findOrDie([
            'id' => $id
        ]);

        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');

        $materials = SearchService::getInstance()
            ->setParserId($parser->id)
            ->run();

        $html = $this->renderPartial('xml', [
            'parser'    => $parser,
            'materials' => $materials
        ]);

        $html = '<?xml version="1.0" encoding="utf-8"?>' . $html;

        return $html;
    }
}
