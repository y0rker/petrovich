<?php
/**
 * User: y0rker
 * Date: 27.11.18
 * Time: 14:13
 */

namespace api\controllers;

use api\components\Controller;
use api\services\SearchService;
use common\models\Material;
use common\models\MaterialPrice;

/**
 * Class MessagesController
 *
 * @package api\controllers\webhook
 */
class SearchController extends Controller
{

    /**
     * @param string $query
     *
     * @return array
     */
    public function actionSearch(string $query = ''): array
    {
        $searchService = new SearchService();
        $searchService->setPage(\Yii::$app->getRequest()->get('page', 1));
        $searchService->setSize(\Yii::$app->getRequest()->get('size', 10));

        return [
            'total_count' => $searchService->getCount(),
            'result'      => $searchService->run(),
            'query'       => $query
        ];
    }

    /**
     * @param int $key
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionPrices(int $key): array
    {
        return MaterialPrice::find()
            ->select([
                MaterialPrice::tableColumn('price'),
                MaterialPrice::tableColumn('created')
            ])
            ->where([
                Material::tableColumn('external_key') => $key
            ])
            ->innerJoin(
                Material::tableName(),
                Material::tableColumn('id') . '=' . MaterialPrice::tableColumn('material_id')
            )
            ->asArray()
            ->all();
    }
}
