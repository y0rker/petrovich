<?php

/** @var SearchDto[] $materials */

/** @var Parser $parser */

use common\dto\SearchDto;
use common\models\Parser;

?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?= date('Y-m-d H:i') ?>">
    <name><?= $parser->name ?></name>
    <url><?= $parser->link ?></url>
    <currencies>
        <currency id="RUB" rate="1" />
    </currencies>
    <offers>
        <?php foreach ($materials as $material): ?>
            <offer id="<?= $material->id ?>" available="true">
                <url><?= htmlspecialchars($material->link) ?></url>
                <price><?= $material->price ?></price>
                <name><?= htmlspecialchars($material->name) ?></name>
                <currencyId>RUB</currencyId>
                <picture><?= $material->images ?></picture>
                <vendor><?= htmlspecialchars($material->vendor ?? '') ?></vendor>
                <vendorcode><?= $material->article ?? '' ?></vendorcode>
            </offer>
        <?php endforeach; ?>
    </offers>
</yml_catalog>