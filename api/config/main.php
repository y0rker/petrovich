<?php

use api\components\ApiClient;
use api\components\Parsers;
use yii\web\User;

$params = array_merge(
    require __DIR__ . '/params.php'
);

return [
    'vendorPath'          => dirname(__DIR__, 2) . '/vendor',
    'language'            => 'ru-RU',
    'id'                  => 'forum-api',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'modules'             => [],
    'defaultRoute'        => 'search/search',
    'components'          => [
        'user'       => [
            'class'         => \yii\web\User::class,
            'identityClass' => User::class,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => array(
                '' => 'search/search',
                'prices'    => 'search/prices'
            ),
        ]
    ],
    'params'              => $params,
];
