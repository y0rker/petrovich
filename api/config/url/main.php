<?php
/**
 * User: y0rker
 * Date: 23.07.18
 * Time: 14:31
 */

return [
    'GET v<ver:\d>/<module>/<ctrl>/<id:\d+>'       => 'v<ver>/<module>/<ctrl>/view',
    'POST v<ver:\d>/<module>/<ctrl>'               => 'v<ver>/<module>/<ctrl>/create',
    'POST v<ver:\d>/<module>/<ctrl>/<id:\d+>'      => 'v<ver>/<module>/<ctrl>/create',
    'PUT,PATCH v<ver:\d>/<module>/<ctrl>'          => 'v<ver>/<module>/<ctrl>/update',
    'PUT,PATCH v<ver:\d>/<module>/<ctrl>/<id:\d+>' => 'v<ver>/<module>/<ctrl>/update',
    'DELETE v<ver:\d>/<module>/<ctrl>/<id:\d+>'    => 'v<ver>/<module>/<ctrl>/delete',
    'GET v<ver:\d>/<module>/<ctrl>'                => 'v<ver>/<module>/<ctrl>/index',
    'v<ver:\d>/<module>/<ctrl>/<action>/<id:\d+>'  => 'v<ver>/<module>/<ctrl>/<action>',

    'GET v<ver:\d>/<ctrl>'                => 'v<ver>/<ctrl>/index',
    'GET v<ver:\d>/<ctrl>/<id:\d+>'       => 'v<ver>/<ctrl>/view',
    'POST v<ver:\d>/<ctrl>'               => 'v<ver>/<ctrl>/create',
    'POST v<ver:\d>/<ctrl>/<id:\d+>/'     => 'v<ver>/<ctrl>/create',
    'PUT,PATCH v<ver:\d>/<ctrl>/<id:\d+>' => 'v<ver>/<ctrl>/update',
    'DELETE v<ver:\d>/<ctrl>/<id:\d+>'    => 'v<ver>/<ctrl>/delete',

    'v<ver:\d>/<module>/<ctrl>/<act>' => 'v<ver>/<module>/<ctrl>/<act>',
];