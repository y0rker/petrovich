<?php
/**
 * User: y0rker
 * Date: 12.12.18
 * Time: 18:08
 */

namespace api\interfaces;

use common\services\ParserServiceBase;

/**
 * Interface ParserInterface
 * @package api\interfaces
 */
interface ParserInterface
{
    /**
     * @param string $id service id.
     */
    public function setId(string $id): void;

    /**
     * @return string service id
     */
    public function getId(): string;

    /**
     * @return string service name
     */
    public function getName(): string;

    /**
     * @return string service url.
     */
    public function getBaseUrl(): string;

    /**
     * @param string $url service url.
     */
    public function setBaseUrl(string $url): void;

    /**
     * @return ParserServiceBase.
     */
    public function getService(): ParserServiceBase;
}