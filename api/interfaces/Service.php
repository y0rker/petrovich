<?php
/**
 * Created by PhpStorm.
 * Module: andkon
 * Date: 23.03.17
 * Time: 13:36
 */

namespace api\interfaces;

/**
 * Interface Service
 *
 * @package common\interfaces
 */
interface Service
{
    /**
     * @return static
     */
    public static function getInstance();

    /**
     * @return mixed
     */
    public function run();
}
