<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 04.09.17
 * Time: 18:07
 */

namespace api\components;

use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class Controller
 *
 * @package api\components
 */
class Controller extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        $parent = parent::beforeAction($action);

        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        if ($data) {
            \Yii::$app->getRequest()->setBodyParams($data);
        }

        return $parent;
    }

    /**
     * @inheritdoc
     */
    protected function verbs(): array
    {
        return [
            'hook' => ['POST'],
        ];
    }
}
