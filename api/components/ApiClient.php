<?php
/**
 * User: y0rker
 * Date: 30.11.18
 * Time: 13:30
 */

namespace api\components;

use api\interfaces\ParserInterface;
use common\services\ParserServiceBase;
use Throwable;
use yii\base\Component;
use yii\web\BadRequestHttpException;

/**
 * Class Client
 * @package api\components
 */
class ApiClient extends Component implements ParserInterface
{
    /** @var string */
    private $_id;
    /** @var string */
    private $_baseUrl;
    /** @var string */
    private $_ref;
    /** @var string */
    private $_name;

    /** @var array */
    private $_categories;
    /** @var int delay for usleep() */
    private $_delay = 500000;

    /**
     * @return string service id
     */
    public function getId(): string
    {
        return $this->_id;
    }

    /**
     * @param string $id service id.
     */
    public function setId(string $id): void
    {
        $this->_id = $id;
    }

    /**
     * @return string service url.
     */
    public function getBaseUrl(): string
    {
        return $this->_baseUrl;
    }

    /**
     * @param string $url service url.
     */
    public function setBaseUrl(string $url): void
    {
        $this->_baseUrl = $url;
    }

    /**
     * @return ParserServiceBase
     * @throws BadRequestHttpException
     */
    public function getService(): ParserServiceBase
    {
        $serviceName = ucfirst($this->_id) . 'Service';
        $serviceName = 'common\\services\\parsers\\Parser' . $serviceName . '\\' . $serviceName;
        try {
            $classModel = new $serviceName;
        } catch (Throwable $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        if (!$classModel instanceof ParserServiceBase) {
            throw new BadRequestHttpException('Парсер не ' . ParserServiceBase::class);
        }

        $classModel->setClient($this);

        return $classModel;
    }

    /**
     * @return string
     */
    public function getRef(): string
    {
        return $this->_ref ?? '';
    }

    /**
     * @param string $ref
     */
    public function setRef(string $ref): void
    {
        $this->_ref = $ref;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->_categories ?? [];
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories): void
    {
        $this->_categories = $categories;
    }

    /**
     * @return int
     */
    public function getDelay(): int
    {
        return $this->_delay;
    }

    /**
     * @param int $delay
     */
    public function setDelay(int $delay): void
    {
        $this->_delay = $delay;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->_name = $name;
    }
}
