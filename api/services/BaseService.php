<?php
/**
 * Created by PhpStorm.
 * Module: andkon
 * Date: 23.03.17
 * Time: 13:49
 */

namespace api\services;

/**
 * Class BaseService
 *
 * @package common\modules\user\services
 */
abstract class BaseService
{
    /** @var  static */
    static protected $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        $class = static::class;
        if (!self::$instance || !(self::$instance instanceof $class)) {
            self::$instance = new static();
        }

        return self::$instance;
    }
}
