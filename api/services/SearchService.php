<?php
/**
 * User: y0rke
 * Date: 09.02.2019
 * Time: 11:33
 */

namespace api\services;

use api\interfaces\Service;
use common\dto\SearchDto;
use common\helpers\ParserHelper;
use common\models\Category;
use common\models\Material;
use common\models\MaterialPrice;
use PDO;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class SearchService
 *
 * @package api\services
 */
class SearchService extends BaseService implements Service
{
    /** @var int */
    protected $page = 1;
    /** @var int */
    protected $size = 10;
    /** @var int */
    protected $parser_id;

    /**
     * @return array
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function run(): array
    {
        $subQuery = (new Query())
            ->select(MaterialPrice::tableColumn('price'))
            ->from(MaterialPrice::tableName())
            ->where([
                MaterialPrice::tableColumn('material_id') => new Expression(Material::tableColumn('id'))
            ])
            ->orderBy(MaterialPrice::tableColumn('created') . ' DESC')
            ->limit(1);
        $subCreatedQuery = (new Query())
            ->select(MaterialPrice::tableColumn('created'))
            ->from(MaterialPrice::tableName())
            ->where([
                MaterialPrice::tableColumn('material_id') => new Expression(Material::tableColumn('id'))
            ])
            ->orderBy(MaterialPrice::tableColumn('created') . ' DESC')
            ->limit(1);
        $query = Material::find()
            ->select([
                Material::tableColumn('id'),
                Material::tableColumn('external_key'),
                Material::tableColumn('name'),
                Material::tableColumn('preview'),
                Material::tableColumn('link'),
                Material::tableColumn('images'),
                Material::tableColumn('vendor'),
                Material::tableColumn('article'),
                'price'                 => $subQuery,
                'last_update'           => $subCreatedQuery,
                'category_name'         => Category::tableColumn('name'),
                'category_external_key' => Category::tableColumn('external_key')
            ])
            ->leftJoin(
                Category::tableName(),
                Category::tableColumn('id') . '=' . Material::tableColumn('category_id')
            );

        if (!empty($this->parser_id)) {
            $query->andWhere([
                Material::tableColumn('parser_id') => $this->parser_id
            ]);
        } else {
            $query
                ->limit($this->size)
                ->offset($this->page * $this->size);
        }

        /** @var SearchDto[] $materials */
        $materials = $query
            ->createCommand()
            ->queryAll([PDO::FETCH_CLASS, SearchDto::class]);

        foreach ($materials as &$material) {
            $categoryUrl = ParserHelper::CATALOG_URL . $material->category_petrovich_key . '/';
            $url = $categoryUrl . $material->petrovich_key . '/';
            $material->url = $url;
            $material->category_url = $categoryUrl;
        }

        return $materials;
    }

    /**
     * @return int
     * @throws InvalidConfigException
     */
    public function getCount(): int
    {
        return Material::find()->count();
    }

    /**
     * @param int $page
     *
     * @return SearchService
     */
    public function setPage(int $page): SearchService
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @param int $size
     *
     * @return SearchService
     */
    public function setSize(int $size): SearchService
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @param int $parser_id
     * @return SearchService
     */
    public function setParserId(int $parser_id): SearchService
    {
        $this->parser_id = $parser_id;

        return $this;
    }
}
